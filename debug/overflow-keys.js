/**
 * Chromeストレージを使用項目数でパンクさせるスクリプト.
 *
 * appendKeyCountを適当に調整してから実行してください.
 */
storage = chrome.storage.sync;
appendKeyPrefix = 'overflow-';
appendKeyCount = 500;

storage.get(function(e) {
    keys = [];
    $.each(e, function(key, value) {
        if (key.startsWith(appendKeyPrefix)) {
            keys.push(key);
        }
    });
    storage.remove(keys, function() {
        keys = {};
        for (i = 0; i < appendKeyCount; i++) {
            keys[appendKeyPrefix + i] = {};
        }
        storage.set(keys, function() {
            storage.get(function(e) {
                console.log(e);
            });
        });
    });
});
