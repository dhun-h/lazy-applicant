/**
 * Chromeストレージを使用データ量でパンクさせるスクリプト.
 *
 * appendDataSizeを適当に調整してから実行してください.
 */
storage = chrome.storage.local; // local or sync
appendKeyPrefix = 'overflow-';
appendDataSize = 5000 * 1024 - 200;

storage.get(function(e) {
    keys = [];
    $.each(e, function(key, value) {
        if (key.startsWith(appendKeyPrefix)) {
            keys.push(key);
        }
    });
    storage.remove(keys, function() {
        entity = {};
        for (i = 0; i < 10000; i++) {
            key = appendKeyPrefix + i;
            value = '0'.repeat(512);
            entity[key] = value;
            if (JSON.stringify(entity).length > appendDataSize) {
                break;
            }
        }

        storage.set(entity, function() {
            storage.get(function(e) {
                console.log(e);
            });
        });
    });
});
