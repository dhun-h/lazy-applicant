/**
 * Chromeストレージから、プロジェクト設定をコンソール出力するスクリプト.
 *
 * targetSettingKeyとtargetTrackerIdを適当に調整してから実行してください.
 */
storage = chrome.storage.local; // local or sync
targetSettingKey = 'localhost.sample';
targetTrackerId  = '7';

storage.get(targetSettingKey, function(e) {
    $.each(e, function(key, setting) {
        $.each(setting.trackers, function(key, tracker) {
            if (key != targetTrackerId) {
                return true;
            }

            console.log('tracker[' + key + ']->' + tracker.name);

            console.log('  default');
            console.log('    trackerTemplate->' + tracker.defaultTrackerTemplate.use + ', ' + tracker.defaultTrackerTemplate.val);

            console.log('  templateNameOfLastUse->' + tracker.templateNameOfLastUse);
            console.log('  templates');
            $.each(tracker.templates, function(key, template) {
                console.log('    ' + template.useCount + '->' + template.name);
            });
        });

        console.log('usedAssignerIdOfLastUse->' + setting.usedAssignerIdOfLastUse);
        console.log('usedAssigners');
        $.each(setting.usedAssigners, function() {
            console.log('  ' + this.useCount + '->' + this.name);
        });

        console.log('watcherTemplateNameOfLastUse->' + setting.watcherTemplateNameOfLastUse);
        console.log('watcherTemplates');
        $.each(setting.watcherTemplates, function() {
            console.log('    ' + this.useCount + '->' + this.name);
        });
    });
});
