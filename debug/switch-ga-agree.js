/**
 * Chromeストレージから、プロジェクト設定をコンソール出力するスクリプト.
 *
 * targetSettingKeyとtargetTrackerIdを適当に調整してから実行してください.
 */
storage = chrome.storage.local; // local or sync
agreeGa = false;

storage.get('global', function(items) {
    globalSetting = items.global || {};
    globalSetting.agreeGa = agreeGa;
    storage.set(items, function() {
        storage.get('global', function(items) {
            console.log(items.global);
        });
    });
});
