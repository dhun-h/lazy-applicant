#!/bin/bash

pushd $(cd $(dirname $0);pwd) > /dev/null

if [ ! -e setting ]; then
    echo settingファイルが見つかりません
    exit 9
fi

. setting
$REDMINE_HOME/ctlscript.sh stop
