redmineではテーマ色を変更しているらしく、
jquery-ui-1.11.0をダウンロードしても外観が同じにならなかった.

redmine-2.6.6-0から次のファイル群を拝借することで対処した.
  redmine-2.6.6-0/apps/redmine/htdocs/public/stylesheets/jquery/images/*
  redmine-2.6.6-0/apps/redmine/htdocs/public/stylesheets/jquery/jquery-ui-1.11.0.css
