/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
/*eslint-disable no-var */
!function() {
    'use strict';

    var curSetting = null;
    var curTracker = null;

    lazyApp.OptionPage = {

        ready: function() {
            setupI18n();

            lazyApp.UiFuncs.prepareBalloonDefaults();

            lazyApp.GaFuncs.pushGaPageViewedOnBackground(location.pathname);
            lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_OPTION_PAGE, '初期表示');

            if (ENABLE_GOOGLE_ANALYTICS) {
                $('#agreeGaContainer').css('display', 'inherit');
            }

            $('input:radio[name="featureType"]').on('change', onChangeFeature);
            $('#store').on('click', storeOptions);
            $('#reset').on('click', resetOptions);
            $('#remove').on('click', removeOptions);

            $('#featureTypeTrackerTemplateOnly').val(FEATURE_TYPE_TRACKER_TEMPLATE_ONLY);
            $('#featureTypeAll').val(FEATURE_TYPE_ALL);

            $('#listSortTypeLastFrequency').val(LIST_SORT_TYPE_LAST_FREQUENCY);
            $('#listSortTypeOnlyFrequency').val(LIST_SORT_TYPE_ONLY_FREQUENCY);

            $('.lazyApplicant_icon').each(function() {
                lazyApp.UiFuncs.applyBalloon($(this), {}, GA_CATEGORY_OPTION_PAGE);
            });

            initContent();
        }
    };

    function setupI18n() {

        // XXX この時点で属性変更しても効果あるのか？
        $('html').attr('lang', chrome.i18n.getMessage('html_lang'));

        $('.i18n-label').each(function() {
            var element = $(this);
            var msg = chrome.i18n.getMessage(element.attr('i18n-key'));
            var children = element.children();
            element.text(msg);
            children.each(function() {
                element.append(this);
            });
        });

        var param = {};
        param['.i18n-hint'] = 'hint';
        param['.i18n-link'] = 'href';
        $.each(param, function(clazz, attr) {
            $(clazz).each(function() {
                var element = $(this);
                var key = element.attr('i18n-key');
                element.attr(attr, chrome.i18n.getMessage(key));
            });
        });
    }

    function initExpandFeatures() {
        var featureType = $('input:radio[name="featureType"]:checked').val();
        if (featureType == FEATURE_TYPE_TRACKER_TEMPLATE_ONLY) {
            $('.' + CLASS_EXPAND_FEATURE).addClass(CLASS_DISABLED_FEATURE);
        } else {
            $('.' + CLASS_EXPAND_FEATURE).removeClass(CLASS_DISABLED_FEATURE);
        }
    }

    function onChangeFeature() {
        $('.' + CLASS_EXPAND_FEATURE).toggle('clip', {}, 1000);
    }

    function initContent() {
        lazyApp.DbFuncs.findGlobalSetting(function(globalSetting) {
            setupSettingKeys(globalSetting);
            initContentBySettingKey();

            $('input:radio[name="featureType"]').val([ globalSetting.featureType ]);
            $('#agreeGa').prop('checked', globalSetting.agreeGa);

            initExpandFeatures();
        });
    }

    function setupSettingKeys(globalSettings) {
        var paramMap = lazyApp.Funcs.mapUrlParameter();
        var key = ('defaultKey' in paramMap ? paramMap.defaultKey : null);

        var generate = function(val, title) {
            return $('<option>')
                    .val(val)
                    .attr('selected', val == key ? true : false)
                    .append(title ? title : val);
        };

        var select = $('#settingKeys');
        select.empty();

        if (globalSettings.settingKeys.length) {
            $.each(globalSettings.settingKeys, function() {
                select.append(generate(this));
            });
        } else {
            select.append(generate(null, chrome.i18n.getMessage('option_choice_noSetting')));
        }

        select.prop('disabled', (globalSettings.settingKeys.length == 0));
        select.on('change', initContentBySettingKey);
    }

    function initContentBySettingKey(trackerId) {
        var action = function(setting) {
            $.each(setting.trackers, function() {
                $.each(this.templates, function() {
                    this.use = true; // 永続化しないフィールドのため個別機能側で初期化
                });
            });

            setupTrackers(setting.trackers, trackerId);
            var tracker = resolveSelectedTracker(setting);

            setupContentBySetting(setting);
            setupContentByTracker(setting, tracker);

            curSetting = setting;
            curTracker = tracker;
        };

        var key = resolveSelectedSettingKey();
        if (key) {
            lazyApp.DbFuncs.findSetting(key, action);
        } else {
            action({
                trackers: {},
                assigners: {},
                usedAssigners: [],
                watcherTemplates: []
            });
        }
    }

    function resolveSelectedSettingKey() {
        return $('#settingKeys').val();
    }

    function resolveSelectedTracker(setting) {
        var trackerId = $('#trackers').val();
        return setting.trackers[trackerId];
    }

    function setupTrackers(trackers, trackerId) {
        var select = $('#trackers');
        select.empty();

        if (Object.keys(trackers).length > 0) {
            for (var key in trackers) {
                var tracker = trackers[key];
                var option = $('<option>')
                        .val(tracker.id)
                        .attr('selected', (trackerId ? trackerId == key : false ))
                        .append(tracker.name);
                select.append(option);
            }
        } else {
            var option = $('<option>')
                    .val(null)
                    .append(chrome.i18n.getMessage('option_choice_noSetting'));
            select.append(option);
        }

        select.on('change', onChangeTracker);
    }

    function onChangeTracker() {
        storeTrackerSettingToCurrent();

        var tracker = resolveSelectedTracker(curSetting);
        setupContentByTracker(curSetting, tracker);

        curTracker = tracker;
    }

    function storeTrackerSettingToCurrent() {
        if (curTracker) {
            curTracker.defaultTrackerTemplate.val = $('#valDefaultTrackerTemplate').val();
            curTracker.defaultTrackerTemplate.use = (curTracker.defaultTrackerTemplate.val != '');

            $.each(curTracker.templates, function() {
                this.use = $('input:checkbox[name="trackerTemplateCheck"][value="' + this.name + '"]').prop('checked');
            });
        }
    }

    function setupContentBySetting(setting) {
        var trackerSelect = $('#tracker');
        var trackerTemplates = $('#trackerTemplates');
        var usedAssigners = $('#usedAssigners');
        var watcherTemplates = $('#watcherTemplates');
        var listSortTypes = $('input:radio[name="listSortType"]');
        var storeButton = $('#store');
        var resetButton = $('#reset');
        var removeButton = $('#remove');

        // disabled
        var disabled = (setting.key ? false : true);

        $.each([trackerSelect,
                trackerTemplates, usedAssigners, watcherTemplates, listSortTypes,
                /*storeButton, */resetButton, removeButton], function() {
            this.prop('disabled', disabled);
        });

        // checkbox
        setupTrackerTemplateCheckboxes(setting);
        setupUsedAssignerCheckboxes(setting);

        // initial value
        if (setting) {
            listSortTypes.val([ setting.listSortType ]);
        } else {
            listSortTypes.prop('checked', false);
        }

        setupUsageStatus();
    }

    function setupContentByTracker(setting, tracker) {
        var trackerSelect = $('#trackers');
        var valDefaultTrackerTemplate = $('#valDefaultTrackerTemplate');

        // disabled
        var disabled = (tracker ? false : true);

        $.each([trackerSelect, valDefaultTrackerTemplate], function() {
            this.prop('disabled', disabled);
        });

        // options
        setupTrackerTemplteOptions(tracker);

        // checkbox
        setupWatcherTemplateCheckboxes(tracker);

        // initial value
        /*eslint-disable no-multi-spaces */
        valDefaultTrackerTemplate.val(tracker && tracker.defaultTrackerTemplate.use ?
                                                 tracker.defaultTrackerTemplate.val : null);
        /*eslint-enable no-multi-spaces */

        // sync
        // ->noop
    }

    function setupTrackerTemplteOptions(tracker) {
        var choices = [];
        var defaultSetting = null;

        if (tracker) {
            $.each(tracker.templates, function() {
                choices.push({
                    value: this.name,
                    title: this.name
                });
            });
            defaultSetting = tracker.defaultTrackerTemplate;
        }

        setupOptions('#valDefaultTrackerTemplate', choices, defaultSetting);
    }

    function setupOptions(id, choices, defaultSetting) {
        var defaultVal = defaultSetting && defaultSetting.use ?
                         defaultSetting.val : null;

        var generate = function(value, title) {
            return $('<option>')
                    .val(value)
                    .attr('selected', value == defaultVal ? true : false)
                    .append(title ? title : value);
        };

        var select = $(id);
        select.empty();
        select.append(generate(null, ''));
        $.each(choices, function() {
            select.append(generate(this.value, this.title));
        });
    }

    function setupTrackerTemplateCheckboxes(setting) {
        var choices = [];
        if (setting) {
            $.each(setting.usedAssigners, function() {
                var assigner = setting.assigners[this.id];
                choices.push({
                    id: this.id,
                    name: assigner.name,
                    use: true
                });
            });
        }
        setupCheckboxes('#usedAssigners', 'usedAssignerCheck', choices);
    }

    function setupUsedAssignerCheckboxes(setting) {
        var choices = [];
        if (setting) {
            $.each(setting.watcherTemplates, function() {
                choices.push({
                    id: this.name,
                    name: this.name,
                    use: true
                });
            });
        }
        setupCheckboxes('#watcherTemplates', 'watcherTemplateCheck', choices);
    }

    function setupWatcherTemplateCheckboxes(tracker) {
        var choices = [];
        if (tracker) {
            $.each(tracker.templates, function() {
                choices.push({
                    id: this.name,
                    name: this.name,
                    use: this.use
                });
            });
        }
        setupCheckboxes('#trackerTemplates', 'trackerTemplateCheck', choices);
    }

    function setupCheckboxes(id, name, choices) {
        var select = $(id);
        select.empty();

        if (choices.length) {
            var appendCheck = function(index, choice) {
                var id = name + '_' + index;
                var check = $('<input type="checkbox">')
                        .val(choice.id)
                        .attr('id', id)
                        .attr('name', name)
                        .prop('checked', choice.use);
                var label = $('<label>')
                        .attr('for', id)
                        .append(choice.name);
                var span = $('<span>')
                        .append(check)
                        .append(label);

                select.append($('<li>').append(span));

                return {
                    check: check,
                    label: label,
                    span: span
                };
            };

            var allCheck = appendCheck('-1', {
                id: '',
                use: false,
                name: null
            });
            allCheck.span.addClass('all-check');

            var checks = [];
            $.each(choices, function(index, choice) {
                checks.push(appendCheck(index, choice).check);
            });

            var changeAllCheckState = function() {
                var isAllCheck = true;
                $.each(checks, function() {
                    if (this.prop('checked') == false) {
                        isAllCheck = false;
                        return false;
                    }
                });

                var title;
                if (isAllCheck) {
                    title = chrome.i18n.getMessage('option_label_allCheck_uncheck');
                } else {
                    title = chrome.i18n.getMessage('option_label_allCheck_check');
                }

                allCheck.label.text(title);
                allCheck.check.prop('checked', isAllCheck);
            };

            allCheck.check.on('change', function() {
                $.each(checks, function() {
                    this.prop('checked', allCheck.check.prop('checked'));
                });
                changeAllCheckState();
            });
            $.each(checks, function() {
                this.on('change', changeAllCheckState);
            });

            changeAllCheckState();

        } else {
            var title = chrome.i18n.getMessage('option_choice_noSetting');
            select.append($('<li>').append(title));
        }
    }

    function setupUsageStatus() {
        lazyApp.DbFuncs.findUsageStatus(function(status) {
            var msg = chrome.i18n.getMessage;
            var info = getChromeExtensionInfo();

            var count = status.countInUse;
            var msgCountRatio;
            var msgCountRest;
            var clsCountRest;
            if (info.maxItems < 0) {
                msgCountRatio = '---';
                msgCountRest = msg('option_usage_unlimited');
            } else {
                var countRatio = lazyApp.Funcs.calcRatio(count / info.maxItems);
                var countRest = info.maxItems - count;
                clsCountRest = (countRatio > 90 ? 'warning' : null);
                msgCountRatio = msg('option_usage_ratio', [countRatio]);
                msgCountRest = msg('option_usage_countDesc', [countRest]);
            }

            var bytes = lazyApp.Funcs.round(lazyApp.Funcs.toKiloBytes(status.bytesInUse), 0);
            var msgBytesRatio;
            var msgBytesRest;
            var clsBytesRest;
            if (info.quotaBytes < 0) {
                msgBytesRatio = '---';
                msgBytesRest = msg('option_usage_unlimited');
            } else {
                var bytesRatio = lazyApp.Funcs.calcRatio(status.bytesInUse / info.quotaBytes);
                var bytesRest = lazyApp.Funcs.round(
                        lazyApp.Funcs.toKiloBytes(info.quotaBytes - status.bytesInUse), 0);
                clsBytesRest = (bytesRatio > 90 ? 'warning' : null);
                msgBytesRatio = msg('option_usage_ratio', [bytesRatio]);
                msgBytesRest = msg('option_usage_bytesDesc', [bytesRest]);
            }

            $('#usage-count').css('display', info.isSync ? 'initial' : 'none');
            $('#usage-count-used').text(msg('option_usage_countUsed', [count]));
            $('#usage-count-ratio').text(msgCountRatio).addClass(clsCountRest);
            $('#usage-count-description').text(msgCountRest);

            $('#usage-bytes').css('display', 'initial');
            $('#usage-bytes-used').text(msg('option_usage_bytesUsed', [bytes]));
            $('#usage-bytes-ratio').text(msgBytesRatio).addClass(clsBytesRest);
            $('#usage-bytes-description').text(msgBytesRest);
        });
    }

    function storeOptions() {
        // global setting
        var featureType = $('input:radio[name="featureType"]:checked').val();
        var agreeGa = $('#agreeGa').prop('checked');

        lazyApp.GaFuncs.setAgreeOnBackground(agreeGa);
        lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_OPTION_PAGE, '保存');

        lazyApp.DbFuncs.findGlobalSetting(function(globalSetting) {
            globalSetting.featureType = featureType;
            globalSetting.agreeGa = agreeGa;
            lazyApp.DbFuncs.storeGlobalSetting(globalSetting, function() {
                if (!resolveSelectedSettingKey()) {
                    popupMessage(chrome.i18n.getMessage('option_msg_store'));
                }
            }, lazyApp.UiFuncs.showErrorDialog);
        });

        if (!resolveSelectedSettingKey()) {
            return;
        }

        // トラッカー毎の設定
        storeTrackerSettingToCurrent();

        // 申請した担当者
        var removeAssignerIds = [];
        $('input[name="usedAssignerCheck"]').each(function() {
            var check = $(this);
            if (check.prop('checked') == false) {
                removeAssignerIds.push(check.val());
            }
        });

        // ウォッチャー・テンプレート
        var removeTemplateNames = [];
        $('input:checkbox[name="watcherTemplateCheck"]').each(function() {
            var check = $(this);
            if (check.prop('checked') == false) {
                removeTemplateNames.push(check.val());
            }
        });

        // XXX一覧の並び順
        var listSortType = $('input:radio[name="listSortType"]:checked').val();

        var key = resolveSelectedSettingKey();
        lazyApp.DbFuncs.storeSettingByOptionPage(
                key, curSetting.trackers, removeAssignerIds,
                removeTemplateNames, listSortType, function() {
            var trackerId = $('#trackers').val();
            initContentBySettingKey(trackerId);
            popupMessage(chrome.i18n.getMessage('option_msg_store'));
        }, lazyApp.UiFuncs.showErrorDialog);
    }

    function resetOptions() {
        var msg = chrome.i18n.getMessage('option_msg_resetConfirm');
        showConfirmDialog(msg, function() {
            lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_OPTION_PAGE, 'リセット');

            var key = resolveSelectedSettingKey();
            lazyApp.DbFuncs.resetSetting(key, function() {
                initContentBySettingKey();
                popupMessage(chrome.i18n.getMessage('option_msg_reset'));
            }, lazyApp.UiFuncs.showErrorDialog);
        });
    }

    function removeOptions() {
        var msg = chrome.i18n.getMessage('option_msg_removeConfirm');
        showConfirmDialog(msg, function() {
            lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_OPTION_PAGE, '削除');

            var key = resolveSelectedSettingKey();
            lazyApp.DbFuncs.removeSetting(key, function() {
                initContent();
                popupMessage(chrome.i18n.getMessage('option_msg_remove'));
            }, lazyApp.UiFuncs.showErrorDialog);
        });
    }

    function showConfirmDialog(message, yesAction) {
        var yes = chrome.i18n.getMessage('button_label_yes');
        var non = chrome.i18n.getMessage('button_label_non');
        var option = {
            buttons: {},
            position: {
                my: 'center',
                at: 'center',
                of: $('div.content')
            }
        };
        option.buttons[yes] = function(node) {
            yesAction();
            node.dialog('close');
        };
        option.buttons[non] = function(node) {
            node.dialog('close');
        };

        lazyApp.UiFuncs.showDialog(ID_DIALOG_NODE, message, 1, option);
    }

    function popupMessage(message) {
        var container = $('div.content');
        container.hideBalloon();
        container.attr('title', message)
                 .showBalloon({
                     position: 'center',
                     maxLifetime: 1000
                 });
        container.attr('title', null);
    }
}();

$('body').ready(lazyApp.OptionPage.ready);
