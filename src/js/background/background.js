/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

!function() {
    'use strict';

    var _version;
    var _agree;

    !function() {
        var successBlock = function() {};
        var failureBlock = function(errorInfo) {
            logError(errorInfo);
        };

        // フォアグラウンドとの通信用ポートを準備
        chrome.extension.onConnect.addListener(listenPort);

        lazyApp.DbFuncs.findGlobalSetting(function(globalSetting) {
            // GoogleAnalyticsの準備
            if (ENABLE_GOOGLE_ANALYTICS) {
                _agree = globalSetting.agreeGa;
                resolveManifestVersion(function(manifest) {
                    _version = manifest.version;
                });
                lazyApp.GaFuncs.injectGaScript();
            } else {
                _agree = false;
                _version = null;
            }

            // ARS向けデフォルト・テンプレートの準備
            if (globalSetting.prepareTemplateForArsWorkflowPage == false) {
                lazyApp.DbFuncs.prepareTemplateForArsWorkflowPage(successBlock, failureBlock);
                lazyApp.DbFuncs.storeGlobalSettingByPrepareTemplateForArsWorkflowPage(true, successBlock, failureBlock);
            }
        });
    }();

    function resolveManifestVersion(callback) {
        var url = '/manifest.json';
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            callback(JSON.parse(xhr.responseText));
        };
        xhr.open('GET', url, true);
        xhr.send(null);
    }

    function listenPort(port) {
        var messageListener;
        switch (port.name) {
        case PORT_GA_CHANGE_AGREE:
            messageListener = function(param) {
                _agree = param.agree;
            };
            break;

        case PORT_GA_PAGEVIEW:
            messageListener = function(param) {
                pushGaPageViewed(param);
            };
            break;

        case PORT_GA_TRACK_EVENT:
            messageListener = function(param) {
                pushGaTrackEvent(param);
            };
            break;

        default:
            lazyApp.Funcs.logError('unknown port. name=[' + port.name + ']');
        }

        var disconnectListener = function() {
            port.onMessage.removeListener(messageListener);
            port.onDisconnect.removeListener(disconnectListener);
            lazyApp.Funcs.logDebug('リスナを削除しました. ' + port.name);
        };
        port.onMessage.addListener(messageListener);
        port.onDisconnect.addListener(disconnectListener);
    }

    function pushGaPageViewed(param) {
        if (!_agree) {
            return;
        }

        var pageUrl = param.pageUrl;
        if (_version) {
            pageUrl = '/v' + _version + pageUrl;
        }

        lazyApp.GaFuncs.pushGaPageViewed(pageUrl);
    }

    function pushGaTrackEvent(param) {
        if (!_agree) {
            return;
        }

        lazyApp.GaFuncs.pushGaTrackEvent(param.category, param.action);
    }
}();
