/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
var _gaq = _gaq || [];
/*eslint-disable no-var */
!function() {
    'use strict';

    _gaq.push(['_setAccount', GA_TRACKING_ID]);

    /**
     * lazyApp.GaFuncsのインスタンスには次の種類が存在する
     *   ・background.js ... シングルトン
     *   ・チケット登録画面とオプション画面 ... プロトタイプ
     *
     * GoogleAnalyticsとの通信は、すべてシングルトンインスタンスで行う
     * プロトタイプスコープのインスタンスは、xxxOnBackgroundメソッドを呼び出して処理を委譲すること
     *
     */
    lazyApp.GaFuncs = {

        injectGaScript: function() {
            lazyApp.Funcs.injectElement('script', {
                type: 'text/javascript',
                async: true,
                src: 'https://ssl.google-analytics.com/ga.js'
            });
        },

        pushGaPageViewed: function(pageUrl) {
            _gaq.push(['_trackPageview', pageUrl]);
        },

        pushGaTrackEvent: function(category, action) {
            action = category + '/' + action;
            var label = null;
            var value = null;
            var opt_noninteraction = true;
            _gaq.push(['_trackEvent', category, action,
                       label, value, opt_noninteraction]);

            lazyApp.Funcs.logDebug('pushGaTrackEvent. action=' + action);
        },

        setAgreeOnBackground: function(agree) {
            postMessage(PORT_GA_CHANGE_AGREE, {
                agree: agree
            });
        },

        pushGaPageViewedOnBackground: function(pathname) {
            var pageUrl = pathname;

            // ページビューにサイト情報は含めない方針に変更
            var match = pageUrl.match(/.+\/projects\/.+\/(issues(\/new.*)?)/, '');
            if (match) {
                pageUrl = '/projects/***/' + match[1];
            }

            postMessage(PORT_GA_PAGEVIEW, {
                pageUrl: pageUrl
            });
        },

        pushGaTrackEventOnBackground: function(category, action) {
            postMessage(PORT_GA_TRACK_EVENT, {
                category: category,
                action: action
            });
        }
    };

    function postMessage(portName, param) {
        var port = chrome.extension.connect({ name: portName });
        port.postMessage(param);
        port.disconnect();
    }
}();
