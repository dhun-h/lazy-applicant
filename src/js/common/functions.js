/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
/*eslint-disable no-var */
!function() {
    'use strict';

    // 拡張機能の内部で利用する共通関数
    jQuery.fn.exists = function() {return Boolean(this.length > 0);};

    lazyApp.Funcs = {

        resolveSettingKey: function() {
            var domain = location.hostname.split('.').reverse().join('.');
            var project = location.pathname.match(/projects\/([^\/]+)/)[1];

            return domain + '.' + project;
        },

        mapUrlParameter: function() {
            var params = location.search.substr(1).split('&');
            var result = {};
            $.each(params, function() {
                var kv = this.split('=');
                if (kv.length == 2) {
                    result[kv[0]] = kv[1];
                }
            });
            return result;
        },

        extractElement: function(array, predicate) {
            var result = null;
            $.each(array, function() {
                if (predicate.apply(this)) {
                    result = this;
                    return false;
                }
            });
            return result;
        },

        extractIndex: function(array, predicate) {
            var result = -1;
            $.each(array, function(index, element) {
                if (predicate(this)) {
                    result = index;
                    return false;
                }
            });
            return result;
        },

        extractTemplateByName: function(templates, templateName) {
            return this.extractElement(templates, { apply: function(element) {
                return element.name == templateName;
            }});
        },

        // --------------------------------------------------------------------------------
        // judgement
        isEntryPage: function() {
            // チケット一覧とチケット登録検証エラーのURLは同じ.
            // HTTPメソッドで判断できるがレスポンスからは取得できないため、トラッカーカラムの有無で判定
            return this.isEntryTopPage() || $(ID_TRACKER).exists();
        },

        isEntryTopPage: function() {
            return location.pathname.endsWith('new');
        },

        isArsWorkflowPage: function() {
            var setting = {
                trackers: {}
            };
            $(ID_TRACKER + ' option').each(function() {
                var id = this.value;
                setting.trackers[id] = {
                    id: id,
                    name: this.text
                };
            });

            return this.isArsWorkflowSetting(setting);
        },

        isArsWorkflowSetting: function(setting) {
            var Predicate = function(id, title) {
                this.id = id;
                this.title = title;
            };
            Predicate.prototype.apply = function(element) {
                return element.id == this.id && element.name == this.title;
            };

            /*eslint-disable no-multi-spaces */
            var predicates = [
                new Predicate(TRACKER_ID_UQ      , TRACKER_NM_UQ      ),
                new Predicate(TRACKER_ID_FURIQ   , TRACKER_NM_FURIQ   ),
                new Predicate(TRACKER_ID_DAIQ    , TRACKER_NM_DAIQ    ),
                new Predicate(TRACKER_ID_BIRTHDAY, TRACKER_NM_BIRTHDAY),
                new Predicate(TRACKER_ID_BOOKS   , TRACKER_NM_BOOKS   ),
                new Predicate(TRACKER_ID_JUKEN   , TRACKER_NM_JUKEN   ),
                new Predicate(TRACKER_ID_SEISAN  , TRACKER_NM_SEISAN  ),
                new Predicate(TRACKER_ID_YOYAKU  , TRACKER_NM_YOYAKU  ),
                new Predicate(TRACKER_ID_KOUNYU  , TRACKER_NM_KOUNYU  )
            ];
            /*eslint-enable no-multi-spaces */

            var self = this;
            var result = true;
            $.each(predicates, function() {
                if (!self.extractElement(setting.trackers, this)) {
                    result = false;
                    return false;
                }
            });
            return result;
        },

        // --------------------------------------------------------------------------------
        // injection
        injectTextScript: function(text) {
            this.injectElement('script', {
                type: 'text/javascript',
                textContent: text
            });
        },

        injectTextCss: function(text) {
            this.injectElement('style', {
                type: 'text/css',
                textContent: text
            });
        },

        injectElement: function(element, options) {
            element = document.createElement(element);
            for (var key in options) {
                element[key] = options[key];
            }
            (document.head||document.documentElement).appendChild(element);
          //script.parentNode.removeChild(script); // XXX 要る？
        },

        // --------------------------------------------------------------------------------
        // log
        /*eslint-disable no-console */
        logDebug: function() {
            if (DEBUG)
                console.debug(arguments[0]);
        },
        logInfo: function() {
            if (DEBUG)
                console.info(arguments[0]);
        },
        logWarn: function() {
            if (DEBUG)
                console.warn(arguments[0]);
        },
        logError: function() {
            if (DEBUG)
                console.error(arguments[0]);
        },
        /*eslint-enable no-console */

        // --------------------------------------------------------------------------------
        // etc
        round: function(value, scale) {
            value = value * Math.pow(10, scale);
            value = Math.round(value);
            value = value / Math.pow(10, scale);
            return value;
        },

        toKiloBytes: function(bytes) {
            return bytes / 1024;
        },

        calcRatio: function(value) {
            value = value * 100;
            return this.round(value, 1);
        },

        calcBytes: function(value) {
            return(encodeURIComponent(value).replace(/%../g, 'x').length);
        },

        provideTodays: function() {
            var self = this;

            var result = {};
            $.each(MACRO_SYSTEM_DATE_SET, function() {
                result[this.format] = self.provideToday(this.format);
            });
            return result;
        },

        provideToday: function(format) {
            const MONTHS = [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ];

            var now = new Date();
            var y = '' + (now.getFullYear());
            var m = '' + (now.getMonth() + 1);
            var d = '' + (now.getDate());
            var B = MONTHS[now.getMonth()];
            var b = MONTHS[now.getMonth()].substr(3);
            if (navigator.language == 'ja') {
                B = m + '月';
                b = m + '月';
            }

            if (m.length < 2) m = '0' + m;
            if (d.length < 2) d = '0' + d;

            switch (format) {
            case MACRO_SYSTEM_DATE_ymd_HYPHEN:
                return y + '-' + m + '-' + d;
            case MACRO_SYSTEM_DATE_dmy_SLASH:
                return d + '/' + m + '/' + y;
            case MACRO_SYSTEM_DATE_dmy_DOT:
                return d + '.' + m + '.' + y;
            case MACRO_SYSTEM_DATE_dmy_HYPHEN:
                return d + '-' + m + '-' + y;
            case MACRO_SYSTEM_DATE_mdy_SLASH:
                return m + '/' + d + '/' + y;
            case MACRO_SYSTEM_DATE_dby_SPACE:
                return d + ' ' + b + ' ' + y;
            case MACRO_SYSTEM_DATE_dBy_SPACE:
                return d + ' ' + B + ' ' + y;
            case MACRO_SYSTEM_DATE_bdy_COMMA:
                return b + ' ' + d + ', ' + y;
            case MACRO_SYSTEM_DATE_Bdy_COMMA:
                return B + ' ' + d + ', ' + y;
            default:
                throw new Error('invalid format. value=[' + format + ']');
            }
        }
    };
}();
