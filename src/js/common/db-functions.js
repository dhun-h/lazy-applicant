/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
/*eslint-disable no-var */
!function() {
    'use strict';

    var _chromeExtStorage = getChromeExtensionInfo().storage;

    lazyApp.DbFuncs = {

        // --------------------------------------------------------------------------------
        // init
        initSetting: function(key, setting) {
            if (setting) {
                return setting;
            }

            setting = {
                key: key,
                initialFieldKey: key + '-default',
                trackers: {},
                assigners: {},
                usedAssignerIdOfLastUse: null,
                usedAssigners: [],
                watcherTemplateNameOfLastUse: null,
                watcherTemplates: [],
                listSortType: LIST_SORT_TYPE_LAST_FREQUENCY
            };

            return setting;
        },

        prepareTemplateForArsWorkflowPage: function(successBlock, failureBlock) {
            var self = this;
            var initAction = function(trackerId, subject, description, negotiated) {
                var tracker = {
                    id: trackerId,
                    templates: []
                };

                var template = initTemplate(
                        ARS_DEFAULT_TEMPLATE_SETTING_KEY, ARS_DEFAULT_TEMPLATE_NAME, tracker);

                self.findTrackerTemplateFields(template.id, function(fields) {
                    var initCount = 0;
                    var initAction = function(key, type, value) {
                        if (fields[key] == null) {
                            fields[key] = {
                                type: type,
                                value: value
                            };
                            initCount++;
                        }
                    };

                    initAction(NM_TITLE, 'text', subject);
                    initAction(NM_DESC, 'textarea', description);
                    initAction(NM_NEGOTIATED, 'checkbox', negotiated);

                    initAction(NM_STATUS, 'select', VAL_STATUS_NEW);
                    initAction(NM_PRIORITY, 'select', VAL_PRIORITY_NORMAL);
                    initAction(NM_DELIVERY_ARRANGED, 'select', VAL_DELIVERY_ARRANGED_AFFAIRS);
                    initAction(NM_DELIVERY_DESTINATION, 'select', VAL_DELIVERY_DESTINATION_HOME);
                    initAction(NM_EXAM_PROCEDURE, 'select', VAL_EXAM_PROCEDURE_OWN);

                    if (initCount) {
                        storeInternal(template.id, fields, null, failureBlock);
                    }
                });
            };

            initAction(TRACKER_ID_UQ, '有給休暇申請', null, VAL_NEGOTIATED);
            initAction(TRACKER_ID_FURIQ, '振替休暇申請', null, VAL_NEGOTIATED);
            initAction(TRACKER_ID_DAIQ, '代替休暇申請', null, VAL_NEGOTIATED);
            initAction(TRACKER_ID_BIRTHDAY, '誕生日休暇申請', '誕生月のため', VAL_NEGOTIATED);

            // TRACKER_ID_BOOKS:
            // TRACKER_ID_JUKEN:
            // TRACKER_ID_SEISAN:
            // TRACKER_ID_YOYAKU:
            // TRACKER_ID_KOUNYU:

            if (successBlock) successBlock();
        },

        // --------------------------------------------------------------------------------
        // globalSetting
        findGlobalSetting: function(callback) {
            findGlobalSettingInternal(function(globalSetting) {
                lazyApp.Funcs.logDebug('findGlobalSetting=>');
                lazyApp.Funcs.logDebug(globalSetting);
                callback(globalSetting);
            });
        },

        storeGlobalSetting: function(globalSetting, successBlock, failureBlock) {
            storeGlobalSettingInternal(globalSetting, successBlock, failureBlock);
        },

        storeGlobalSettingBySettingKey: function(key, successBlock, failureBlock) {
            var self = this;
            findGlobalSettingInternal(function(globalSetting) {
                var settingKeys = globalSetting.settingKeys;

                if (0 > $.inArray(key, settingKeys)) {
                    settingKeys.push(key);
                }

                sortSettingKeys(settingKeys);
                storeGlobalSettingInternal(globalSetting, successBlock, failureBlock);
            });
        },

        storeGlobalSettingByAgreeGa: function(agreeGa, successBlock, failureBlock) {
            storeGlobalSettingByBoolean('agreeGa', agreeGa, successBlock, failureBlock);
        },

        storeGlobalSettingByPrepareTemplateForArsWorkflowPage: function(prepared, successBlock, failureBlock) {
            storeGlobalSettingByBoolean('prepareTemplateForArsWorkflowPage', prepared, successBlock, failureBlock);
        },

        // --------------------------------------------------------------------------------
        // project-setting
        findSetting: function(key, callback) {
            findSettingInternal(key, function(setting) {
                lazyApp.Funcs.logDebug('findSetting=>');
                lazyApp.Funcs.logDebug(setting);
                callback(setting);
            });
        },

        findSettingByContentPage: function(
                key, trackerOptions, assignerOptions, callback) {
            var self = this;
            var defaults = {};
            defaults['global'] = null;
            defaults[key] = null;

            _chromeExtStorage.get(defaults, function(items) {
                var globalSetting = items['global'];
                var setting = items[key];

                var isArsWorkflow = lazyApp.Funcs.isArsWorkflowPage();

                setting = self.initSetting(key, setting);

                trackerOptions.each(function() {
                    initTrackerSetting(setting, this.value, this.text, isArsWorkflow);
                });
                assignerOptions.each(function() {
                    initAssignerSetting(setting, this.value, this.text);
                });

                lazyApp.Funcs.logDebug('findSettingByContentPage. [' + key + ']=>');
                lazyApp.Funcs.logDebug(setting);
                callback(globalSetting, setting);
            });
        },

        storeSettingByContentPage: function(
                key, param, successBlock, failureBlock) {

            var self = this;

            findSettingInternal(key, function(setting) {
                var tracker = setting.trackers[param.trackerId];

                // テンプレート
                if (param.trackerTemplateName) {
                    useTrackerTemplate(setting, tracker, param.trackerTemplateName);
                }

                // 担当者
                if (param.usedAssignerId) {
                    useAssigner(setting, param.usedAssignerId);
                }

                // ウォッチャーテンプレート
                if (param.watcherTemplateName) {
                    useWatcherTemplate(setting, param.watcherTemplateName);
                }

                storeInternal(key, setting, successBlock, failureBlock);
            });
        },

        storeSettingByOptionPage: function(
                key, trackers,
                removeAssignerIds, removeWatcherTemplateNames,
                listSortType, successBlock, failureBlock) {

            var self = this;

            findSettingInternal(key, function(setting) {
                var removeTemplateKeys = [];

                setting.listSortType = listSortType;

                $.each(trackers, function() {
                    var tracker = this;
                    var storedTracker = setting.trackers[tracker.id];
                    for (var key in tracker) {
                        switch (key) {
                        case 'defaultTrackerTemplate':
                            storedTracker[key].use = tracker[key].use;
                            storedTracker[key].val = tracker[key].val;
                            break;

                        default:
                            // noop
                        }
                    }

                    $.each(tracker.templates, function() {
                        var template = this;
                        if (template.use) {
                            return true;
                        }

                        if (storedTracker.templateNameOfLastUse == template.name) {
                            storedTracker.templateNameOfLastUse = null;
                        }
                        if (storedTracker.defaultTrackerTemplate.val == template.name) {
                            storedTracker.defaultTrackerTemplate.use = false;
                            storedTracker.defaultTrackerTemplate.val = null;
                        }
                        var idx = lazyApp.Funcs.extractIndex(storedTracker.templates, function(storedTemplate) {
                            return template.id == storedTemplate.id;
                        });
                        if (idx > -1) {
                            storedTracker.templates.splice(idx, 1);
                            removeTemplateKeys.push(template.id);
                        }
                    });
                    sortTrackerTemplates(setting, storedTracker);
                });

                // 利用した担当者
                $.each(removeAssignerIds, function() {
                    var assignerId = this;

                    if (setting.usedAssignerIdOfLastUse == assignerId) {
                        setting.usedAssignerIdOfLastUse = null;
                    }
                    var idx = lazyApp.Funcs.extractIndex(setting.usedAssigners, function(element) {
                        return element.id == assignerId;
                    });
                    if (idx > -1) {
                        setting.usedAssigners.splice(idx, 1);
                    }
                });
                sortUsedAssigners(setting);

                // ウォッチャーパターン
                $.each(removeWatcherTemplateNames, function() {
                    var templateName = this;

                    if (setting.watcherTemplateNameOfLastUse == templateName) {
                        setting.watcherTemplateNameOfLastUse = null;
                    }
                    var idx = lazyApp.Funcs.extractIndex(setting.watcherTemplates, function(element) {
                        return element.name == templateName;
                    });
                    if (idx > -1) {
                        setting.watcherTemplates.splice(idx, 1);
                    }
                });
                sortWatcherTemplates(setting);

                storeInternal(key, setting, function() {
                    self.removeTrackerTemplate(removeTemplateKeys, successBlock, failureBlock);
                }, failureBlock);
            });
        },

        storeSetting: function(key, setting, successBlock, failureBlock) {
            storeInternal(key, setting, successBlock, failureBlock);
        },

        removeSetting: function(key, successBlock, failureBlock) {
            findSettingInternal(key, function(oldSetting) {
                removeSettingInternal(key, oldSetting, successBlock, failureBlock);
            });
        },

        resetSetting: function(key, successBlock, failureBlock) {
            var self = this;

            findSettingInternal(key, function(oldSetting) {
                removeSettingInternal(key, oldSetting, function() {
                    var setting = self.initSetting(key);
                    var isArsWorkflow = lazyApp.Funcs.isArsWorkflowSetting(oldSetting);

                    $.each(oldSetting.trackers, function(key, value) {
                        initTrackerSetting(setting, value.id, value.name, isArsWorkflow);
                    });
                    $.each(oldSetting.assigners, function(key, value) {
                        initAssignerSetting(setting, value.id, value.name);
                    });

                    storeInternal(key, setting, function(newSetting) {
                        self.storeGlobalSettingBySettingKey(key, function() {
                            successBlock(newSetting);
                        }, failureBlock);
                    }, failureBlock);
                }, failureBlock);
            });
        },

        storeWatcherTemplateByContentPage: function(
                key, param, successBlock, failureBlock) {
            var self = this;

            findSettingInternal(key, function(setting) {
                var tracker = setting.trackers[param.trackerId];
                var templates = setting.watcherTemplates;

                if (param.storeTemplateName) {
                    var template = lazyApp.Funcs.extractTemplateByName(templates, param.storeTemplateName);
                    if (template == null) {
                        template = {
                            useCount: 0
                        };
                        templates.push(template);
                    }
                    template.name = param.storeTemplateName;
                    template.ids = param.watcherIds;

                    useWatcherTemplate(setting, param.storeTemplateName);
                }

                storeInternal(key, setting, successBlock, failureBlock);
            });
        },

        findProjectInitialFields: function(setting, callback) {
            var key = setting.initialFieldKey;

            var defaults = {};
            defaults[key] = {};

            _chromeExtStorage.get(
                    defaults,
                    function(items) {
                        var fields = items[key];
                        lazyApp.Funcs.logDebug('findProjectInitialFields. [' + key + ']=>');
                        lazyApp.Funcs.logDebug(fields);
                        callback(fields);
                    }
            );
        },

        storeProjectInitialFields: function(
                setting, newFields, isFlushInitialFields, successBlock, failureBlock) {
            this.findProjectInitialFields(setting, function(curFields) {
                if (isFlushInitialFields) {
                    curFields = newFields;
                } else {
                    $.each(newFields, function(key, value) {
                        if (curFields[key] == null) {
                            curFields[key] = newFields[key];
                        }
                    });
                }

                storeInternal(setting.initialFieldKey, curFields, successBlock, failureBlock);
            });
        },

        // --------------------------------------------------------------------------------
        // project-tracker-template
        findTrackerTemplateFields: function(key, callback) {
            var defaults = {};
            defaults[key] = {};

            _chromeExtStorage.get(
                    defaults,
                    function(items) {
                        var fields = items[key];
                        lazyApp.Funcs.logDebug('findTrackerTemplateFields. [' + key + ']=>');
                        lazyApp.Funcs.logDebug(fields);
                        callback(fields);
                    }
            );
        },

        storeTrackerTemplateByContentPage: function(
                key, param, successBlock, failureBlock) {
            var self = this;

            findSettingInternal(key, function(setting) {
                var tracker = setting.trackers[param.trackerId];
                var templateUpdateType = 0;
                var internalSubSuccessBlock = function(newSetting) {
                    newSetting.templateUpdateType = templateUpdateType; // 揮発性の属性
                    successBlock(newSetting);
                };
                var internalSuccessBlock = function(newSetting) {
                    internalSubSuccessBlock(newSetting);
                };

                // チェックあり＋名前入力済 ... 規定にセット
                // チェックあり＋名前未入力 ... 規定値を無効
                // チェックなし＋名前入力済＋変更前は規定値である ... 規定値を項目
                // チェックなし＋名前入力済＋変更前も規定値でない ... 何もしない
                // チェックなし＋名前未入力 ... 検証エラーなので何もしない
                var action;
                if (param.isDefault) {
                    action = (param.templateName) ? 1 : 2;
                } else if (param.templateName) {
                    action = (param.templateName == tracker.defaultTrackerTemplate.val) ? 2 : 0;
                }

                switch (action) {
                case 1:
                    templateUpdateType |= TEMPLATE_UPDATE_MASK_DEFAULT_SET;
                    tracker.defaultTrackerTemplate.use = true;
                    tracker.defaultTrackerTemplate.val = param.templateName;
                    break;
                case 2:
                    templateUpdateType |= TEMPLATE_UPDATE_MASK_DEFAULT_NON;
                    tracker.defaultTrackerTemplate.use = false;
                    tracker.defaultTrackerTemplate.val = null;
                    break;
                default:
                    // noop
                }

                if (param.templateName) {
                    templateUpdateType |= TEMPLATE_UPDATE_MASK_STORE;
                    var template = lazyApp.Funcs.extractTemplateByName(tracker.templates, param.templateName);
                    if (template == null) {
                        template = initTemplate(setting.key, param.templateName, tracker);
                    }

                    useTrackerTemplate(setting, tracker, param.templateName);

                    var assignerId = param.fields[NM_ASSIGNER] ? param.fields[NM_ASSIGNER].value : null;
                    if (assignerId) {
                        useAssigner(setting, assignerId);
                    }

                    internalSuccessBlock = function(newSetting) {
                        storeInternal(template.id, param.fields, function() {
                            internalSubSuccessBlock(newSetting);
                        }, failureBlock);
                    };
                }
                storeInternal(key, setting, internalSuccessBlock, failureBlock);
            });
        },

        removeTrackerTemplate: function(keys, successBlock, failureBlock) {
            _chromeExtStorage.remove(keys, function() {
                postCrudAction(successBlock, failureBlock);
            });
        },

        // --------------------------------------------------------------------------------
        // temporary
        findTemporary: function(callback) {
            var defaults = {
                temporary: {
                    trackerTemplate: {
                        select: null,
                        check: false,
                        text: null
                    },
                    watcherTemplate: {
                        select: null,
                        check: false,
                        text: null
                    }
                }
            };

            _chromeExtStorage.get(
                    defaults,
                    function(entity) {
                        lazyApp.Funcs.logDebug('findTemporary=>');
                        lazyApp.Funcs.logDebug(entity.temporary);
                        callback(entity.temporary);
                    }
            );
        },

        storeTemporaryByContentPage: function(
                param, successBlock, failureBlock) {
            var temporary = {
                trackerTemplate: {
                    select: param.trackerTemplateSelect,
                    check: param.trackerTemplateCheck,
                    text: param.trackerTemplateText
                },
                watcherTemplate: {
                    select: param.watcherTemplateSelect,
                    text: param.watcherTemplateText
                },
                dateMacros: param.dateMacros
            };
            storeTemporaryInternal(temporary, successBlock, failureBlock);
        },

        // --------------------------------------------------------------------------------
        // quota
        findUsageStatus: function(callback) {
            findBytesInTotalUse(function(bytesInUse) {
                findItemCountInTotalUse(function(countInUse) {
                    callback({
                        bytesInUse: bytesInUse,
                        countInUse: countInUse
                    });
                });
            });
        }
    };

    function storeInternal(key, object, successBlock, failureBlock) {
        var entity = {};
        entity[key] = object;

        _chromeExtStorage.set(
                entity,
                function() {
                    postCrudAction(function() {
                        lazyApp.Funcs.logDebug('storeInternal. [' + key + ']=>');
                        lazyApp.Funcs.logDebug(object);
                        if (successBlock) successBlock(object);
                    }, failureBlock, entity);
                }
        );
    }

    // --------------------------------------------------------------------------------
    // init
    function initTrackerSetting(setting, id, name, isArsWorkflow) {
        if (!id || !name) {
            return;
        }

        var tracker = setting.trackers[id];
        if (tracker) {
            tracker.name = name;
            return tracker;
        }

        tracker = {
            id: id,
            name: name,
            templateNameOfLastUse: null,
            templates: []
        };
        setting.trackers[id] = tracker;

        if (isArsWorkflow) {

            switch (id) {
            case TRACKER_ID_UQ:
                initTemplate(ARS_DEFAULT_TEMPLATE_SETTING_KEY, ARS_DEFAULT_TEMPLATE_NAME, tracker);
                initTrackerSubSetting(tracker, 'defaultTrackerTemplate', true, ARS_DEFAULT_TEMPLATE_NAME);
                break;

            case TRACKER_ID_FURIQ:
                initTemplate(ARS_DEFAULT_TEMPLATE_SETTING_KEY, ARS_DEFAULT_TEMPLATE_NAME, tracker);
                initTrackerSubSetting(tracker, 'defaultTrackerTemplate', true, ARS_DEFAULT_TEMPLATE_NAME);
                break;

            case TRACKER_ID_DAIQ:
                initTemplate(ARS_DEFAULT_TEMPLATE_SETTING_KEY, ARS_DEFAULT_TEMPLATE_NAME, tracker);
                initTrackerSubSetting(tracker, 'defaultTrackerTemplate', true, ARS_DEFAULT_TEMPLATE_NAME);
                break;

            case TRACKER_ID_BIRTHDAY:
                initTemplate(ARS_DEFAULT_TEMPLATE_SETTING_KEY, ARS_DEFAULT_TEMPLATE_NAME, tracker);
                initTrackerSubSetting(tracker, 'defaultTrackerTemplate', true, ARS_DEFAULT_TEMPLATE_NAME);
                break;

            case TRACKER_ID_BOOKS:
            case TRACKER_ID_JUKEN:
            case TRACKER_ID_SEISAN:
            case TRACKER_ID_YOYAKU:
            case TRACKER_ID_KOUNYU:
            default:
                initTrackerSubSetting(tracker, 'defaultTrackerTemplate', false, null);
            }

        } else {
            initTrackerSubSetting(tracker, 'defaultTrackerTemplate', false, null);
        }

        return tracker;
    }

    function initTemplate(settingKey, templateName, tracker) {
        var template = lazyApp.Funcs.extractTemplateByName(tracker.templates, templateName);
        if (template == null) {
            var no = 1;
            $.each(tracker.templates, function() {
                no = (no < this.no + 1) ? this.no + 1 : no;
            });

            template = {
                id: settingKey + '-' + tracker.id + '-' + no,
                no: no,
                name: templateName,
                useCount: 0
            };
            tracker.templates.push(template);
        }

        return template;
    }

    function initTrackerSubSetting(tracker, name, use, val) {
        if (!tracker[name]) tracker[name] = {};
        if (!tracker[name].use) tracker[name].use = use;
        if (!tracker[name].val) tracker[name].val = val;
    }

    function initAssignerSetting(setting, id, name) {
        if (!id || !name) {
            return;
        }

        var assigner = setting.assigners[id];
        if (assigner) {
            assigner.name = name;
            return;
        }

        assigner = {
            id: id,
            name: name
        };
        setting.assigners[id] = assigner;
    }

    // --------------------------------------------------------------------------------
    // globalSetting
    function findGlobalSettingInternal(callback) {
        var entity = {
            global: {
                settingKeys: [],
                featureType: FEATURE_TYPE_TRACKER_TEMPLATE_ONLY,
                agreeGa: true,
                prepareTemplateForArsWorkflowPage: false
            }
        };

        _chromeExtStorage.get(entity, function(items) {
            // since v2.0.3
            items.global.featureType = items.global.featureType || FEATURE_TYPE_ALL;

            callback(items.global);
        });
    }

    function storeGlobalSettingInternal(globalSetting, successBlock, failureBlock) {
        storeInternal('global', globalSetting, successBlock, failureBlock);
    }

    function storeGlobalSettingByBoolean(field, value, successBlock, failureBlock) {
        findGlobalSettingInternal(function(globalSetting) {
            globalSetting[field] = value;
            storeGlobalSettingInternal(globalSetting, successBlock, failureBlock);
        });
    }

    // --------------------------------------------------------------------------------
    // project-setting
    function findSettingInternal(key, callback) {
        var defaults = {};
        defaults[key] = null;

        _chromeExtStorage.get(defaults, function(items) {
            callback(items[key]);
        });
    }

    function removeSettingInternal(key, setting, successBlock, failureBlock) {
        var keys = [];
        keys.push(key);
        keys.push(setting.initialFieldKey);
        $.each(setting.trackers, function() {
            $.each(this.templates, function() {
                keys.push(this.id);
            });
        });

        _chromeExtStorage.remove(keys, function() {
            // ARS向けデフォルト・テンプレートの準備
            var isArsWorkflow = lazyApp.Funcs.isArsWorkflowSetting(setting);
            if (isArsWorkflow) {
                lazyApp.DbFuncs.prepareTemplateForArsWorkflowPage(null, failureBlock);
            }

            findGlobalSettingInternal(function(globalSetting) {
                var idx = lazyApp.Funcs.extractIndex(globalSetting.settingKeys, function(element) {
                    return element == key;
                });
                globalSetting.settingKeys.splice(idx, 1);
                storeGlobalSettingInternal(globalSetting, function() {
                    lazyApp.Funcs.logDebug('removeSettingInternal=>' + key);
                    if (successBlock) successBlock();
                }, failureBlock);
            });
        });
    }

    // --------------------------------------------------------------------------------
    // temporary
    function storeTemporaryInternal(temporary, successBlock, failureBlock) {
        storeInternal('temporary', temporary, successBlock, failureBlock);
    }

    // --------------------------------------------------------------------------------
    // quota
    function findBytesInTotalUse(callback) {
        _chromeExtStorage.getBytesInUse(function(bytesInUse) {
            callback(bytesInUse);
        });
    }

    function findItemCountInTotalUse(callback) {
        _chromeExtStorage.get(function(items) {
            callback(Object.keys(items).length);
        });
    }

    function postCrudAction(successBlock, failureBlock, entity) {
        if (!successBlock) {
            throw new Error('successBlock is missing!');
        }
        if (!failureBlock) {
            throw new Error('failureBlock is missing!');
        }

        var lastError = chrome.runtime.lastError;
        if (lastError) {
            failureBlock(lastError, entity);
        } else {
            successBlock();
        }
    }

    // --------------------------------------------------------------------------------
    // etc
    function useTrackerTemplate(setting, tracker, templateName) {
        tracker.templateNameOfLastUse = templateName;
        var template = lazyApp.Funcs.extractTemplateByName(tracker.templates, templateName);
        incrementUseCount(template);
        sortTrackerTemplates(setting, tracker);
    }

    function useAssigner(setting, assignerId) {
        setting.usedAssignerIdOfLastUse = assignerId;
        var usedAssigner = appendUsedAssignerIfNeed(setting, assignerId);
        incrementUseCount(usedAssigner);
        sortUsedAssigners(setting);
    }

    function useWatcherTemplate(setting, templateName) {
        setting.watcherTemplateNameOfLastUse = templateName;
        var template = lazyApp.Funcs.extractTemplateByName(setting.watcherTemplates, templateName);
        incrementUseCount(template);
        sortWatcherTemplates(setting);
    }

    function appendUsedAssignerIfNeed(setting, id) {
        var predicate = {
            apply: function(element) {
                return element.id == id;
            }
        };
        var assigner = lazyApp.Funcs.extractElement(setting.assigners, predicate);
        var usedAssigner = lazyApp.Funcs.extractElement(setting.usedAssigners, predicate);
        if (usedAssigner == null && assigner) {
            usedAssigner = {
                id: assigner.id,
                useCount: 0
            };
            setting.usedAssigners.push(usedAssigner);
        }
        usedAssigner.name = assigner.name;

        return usedAssigner;
    }

    function incrementUseCount(object) {
        if (object.useCount < Number.MAX_SAFE_INTEGER) {
            object.useCount++;
        }
    }

    function sortSettingKeys(settings) {
        settings.sort(function (o1, o2) {
            if (o1 == o2) {
                return 0;
            } else {
                return o1 > o2 ? +1 : -1;
            }
        });
    }

    function sortBySortType(listSortType, array, valOfLastUse, keyOfLastUse) {
        array.sort(function (o1, o2) {
            if (listSortType == LIST_SORT_TYPE_LAST_FREQUENCY) {
                if (o1[keyOfLastUse] == valOfLastUse) return -1;
                if (o2[keyOfLastUse] == valOfLastUse) return +1;
            }
            return o2.useCount - o1.useCount;
        });
    }

    function sortTrackerTemplates(setting, tracker) {
        sortBySortType(setting.listSortType, tracker.templates, tracker.templateNameOfLastUse, 'name');
    }

    function sortUsedAssigners(setting) {
        sortBySortType(setting.listSortType, setting.usedAssigners, setting.usedAssignerIdOfLastUse, 'id');
    }

    function sortWatcherTemplates(setting) {
        sortBySortType(setting.listSortType, setting.watcherTemplates, setting.watcherTemplateNameOfLastUse, 'name');
    }
}();
