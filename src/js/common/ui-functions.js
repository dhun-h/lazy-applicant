/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
/*eslint-disable no-var */
!function() {
    'use strict';
    lazyApp.UiFuncs = {

        onCheckHintCheckbox: function(event) {
            var checked = event.target.checked;
            var text = event.target.relatedText;

            text.prop('disabled', !checked);

            if (checked) {
                text.val(text.prop('oldValue'));
                text.trigger('blur');
            } else {
                text.prop('oldValue', text.val());
                text.val(null);
            }
        },

        onBluerHintText: function(event) {
            var text = $('#' + event.target.id);
            var value = text.val();
            var title = text.attr('hint');

            if (value && value != title) {
                text.css('color', '#000000');
            } else {
                text.css('color', '#d4bed1');
                text.val(title);
            }
        },

        onFocusHintText: function(event) {
            var text = $('#' + event.target.id);

            if (text.val() == text.attr('hint')) {
                text.val(null);
                text.css('color', '#000000');
            }
        },

        valForHintText: function(id) {
            var text = $(id);
            var value = text.val();
            var title = text.attr('hint');

            if (value && value != title) {
                return value;
            } else {
                return null;
            }
        },

        chooseOptionByName: function(id, name) {
            var options = $(id + ' option');
            var option = options[0];

            $.each(options, function() {
                if (this.text == name) {
                    option = this;
                    return false;
                }
            });
            option.selected = true;
        },

        prepareBalloonDefaults: function() {
            $.balloon.defaults.css.opacity  = 1.0;
            $.balloon.defaults.showDuration = 0;
            $.balloon.defaults.hideDuration = 0;
        },

        createHelpIcon: function(gaValue, hint) {
            var icon = $('<img>')
                    .addClass('lazyApplicant_icon')
                    .attr('src', chrome.extension.getURL('images/question.png'))
                    .attr('ga-value', gaValue)
                    .attr('hint', hint);

            return icon;
        },

        createStoreIcon: function(callback) {
            var icon = $('<img>')
                    .addClass('lazyApplicant_icon')
                    .addClass('lazyApplicant_template_store')
                    .attr('src', chrome.extension.getURL('images/floppy.png'))
                    .on('click', callback);

            return icon;
        },

        createOptionIcon: function(gaCategory, gaValue, settingKey) {
            var action = function() {
            // if (chrome.runtime.openOptionsPage) {
            //     chrome.runtime.openOptionsPage();
            // } else {

                window.open(URL_OPTION_PAGE + '?defaultKey=' + settingKey, 'option-page');
            // }
                lazyApp.GaFuncs.pushGaTrackEventOnBackground(gaCategory, 'オプション画面遷移/' + gaValue);
            };

            var icon = $('<img>')
                    .addClass('lazyApplicant_icon')
                    .attr('src', chrome.extension.getURL('images/gear.png'))
                    .on('click', action);

            return icon;
        },

        applyBalloon: function(element, option, gaCategory) {
            var opt = {
                tipSize: 0
            };

            var position = element.attr('balloon-pos');
            switch (position) {
            case 'bottom':
                opt.position = position;
                opt.offsetY = -12;
                break;
            case 'top':
            default:
                opt.position = 'top';
                opt.offsetY = +12;
                break;
            }
            opt.offsetX = element.attr('balloon-offset-x') ? parseInt(element.attr('balloon-offset-x')) : opt.offsetX;
            opt.offsetY = element.attr('balloon-offset-y') ? parseInt(element.attr('balloon-offset-y')) : opt.offsetY;

            option = option || {};
            $.each(option, function(key, value) {
                opt[key] = value;
            });

            element.on('click', function() {
                lazyApp.GaFuncs.pushGaTrackEventOnBackground(gaCategory, 'ヒント表示/' + element.attr('ga-value'));

                if (opt) {
                    element.attr('title', element.attr('hint'));
                }
                element.showBalloon(opt);
                option = null;
            });
            element.on('mouseout', function() {
                $(this).hideBalloon();
            });
        },

        showErrorDialog: function(errorInfo, entity) {
            lazyApp.Funcs.logError(errorInfo);

            var info = getChromeExtensionInfo();
            var msg;
            if (errorInfo.message.startsWith('MAX_ITEMS')) {
                msg = chrome.i18n.getMessage('msg_error_MAX_ITEMS', [info.maxItems]);

            } else if (errorInfo.message.startsWith('QUOTA_BYTES_PER_ITEM')) {
                var quota = info.quotaBytesPerItem;
                var bytes = '???';
                if (entity) {
                    $.each(entity, function(key, value) {
                        bytes = lazyApp.Funcs.calcBytes(JSON.stringify(key))
                              + lazyApp.Funcs.calcBytes(JSON.stringify(value))
                              - 2 /*value全体の{}の部分*/;
                        return false;
                    });
                }
                msg = chrome.i18n.getMessage('msg_error_QUOTA_BYTES_PER_ITEM', [quota, bytes]);

            } else if (errorInfo.message.startsWith('QUOTA_BYTES')) {
                var quota = lazyApp.Funcs.round(
                        lazyApp.Funcs.toKiloBytes(info.quotaBytes), 0);
                msg = chrome.i18n.getMessage('msg_error_QUOTA_BYTES', [quota]);

            } else {
                msg = chrome.i18n.getMessage('msg_error_UNKNOWN');
            }

            lazyApp.UiFuncs.showDialog(ID_DIALOG_NODE, msg);
        },

        showDialog: function(id, message, buttonIndex, option) {
            var node = $(id);

            var opt = {
                modal: true,
                autoOpen: false,
                resizable: false,
                draggable: false,
                show: 'blind',
                hide: 'explode',
                dialogClass: CLASS_DIALOG,
                open: function() {
                    $(this).siblings('.ui-dialog-buttonpane')
                           .find('button:eq(' + buttonIndex + ')')
                           .focus();
                },
                buttons: {}
            };

            var ok = chrome.i18n.getMessage('button_label_ok');
            opt.buttons[ok] = function() {
                node.dialog('close');
            };

            for (var key in option) {
                if (key == 'buttons') {
                    opt[key] = {};
                    for (var title in option[key]) {
                        opt[key][title] = function(ev) {
                            title = $(ev.target).text();
                            option['buttons'][title](node);
                        };
                    }
                } else {
                    opt[key] = option[key];
                }
            }

            node.dialog(opt);
            node.html(message).dialog('open');

            return node;
        },

        popupMessage: function(containerId, message, option) {
            var opt = {
                position: 'center',
                maxLifetime: 1000
            };
            for (var key in option) {
                opt[key] = option[key];
            }

            var container = $(containerId);
            container.hideBalloon();
            container.attr('title', message)
                     .showBalloon(opt);
            container.attr('title', null);
        },

        appendWatcherCheckbox: function(assigner) {
            if (assigner == null) {
                return true;
            }

            var inputs = $(ID_WATCHERS_INPUTS);
            var check = $('<input>')
                    .attr('type', 'checkbox')
                    .attr('name', NM_WATCHER_CHECK)
                    .val(assigner.id);
            var label = $('<label>')
                    .attr('id', ID_PREFIX_WATCHER_LABEL + assigner.id)
                    .attr('class', 'floating')
                    .append(check)
                    .append(assigner.name);

            inputs.append(label);
        }
    };
}();
