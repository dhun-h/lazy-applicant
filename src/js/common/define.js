/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-multi-spaces, no-multiple-empty-lines, strict */

// Redmineの識別子(変更不可)
// for all redmine
const ID_REPLACEMENT_ROOT = '#all_attributes';
const ID_ISSUE_FORM = '#issue-form';
const ID_TRACKER = '#issue_tracker_id';
const ID_TITLE = '#issue_subject';
const ID_DESC  = '#issue_description';
const ID_STATUS  = '#issue_status_id';
const ID_ASSIGNER = '#issue_assigned_to_id';
const ID_START_DATE = '#issue_start_date';
const ID_DUE_DATE = '#issue_due_date';
const ID_WATCHERS_FORM = '#watchers_form';
const ID_WATCHERS_INPUTS = '#watchers_inputs';
const ID_PREFIX_WATCHER_LABEL = 'issue_watcher_user_ids_';
const ID_DATE_FIELD_SET = [ID_START_DATE.substr(1), ID_DUE_DATE.substr(1)];

const NM_TITLE = 'issue[subject]';
const NM_DESC  = 'issue[description]';
const NM_STATUS = 'issue[status_id]';
const NM_ASSIGNER = 'issue[assigned_to_id]';
const NM_PRIORITY = 'issue[priority_id]';
const NM_WATCHER_CHECK = 'issue[watcher_user_ids][]';

const DATE_FIELD_CLASS = 'hasDatepicker';
const DATE_PICKER_BUTTON_CLASS = 'ui-datepicker-trigger';


// for ars workflow redmine
const TRACKER_ID_UQ       = '7';  // 有給休暇
const TRACKER_ID_FURIQ    = '8';  // 振替休暇
const TRACKER_ID_DAIQ     = '9';  // 代替休暇
const TRACKER_ID_BIRTHDAY = '10'; // 誕生日休暇
const TRACKER_ID_BOOKS    = '11'; // 書籍購入
const TRACKER_ID_JUKEN    = '13'; // 資格受験
const TRACKER_ID_SEISAN   = '14'; // 立替精算
const TRACKER_ID_YOYAKU   = '15'; // 会議室依頼
const TRACKER_ID_KOUNYU   = '12'; // 機器購入

const TRACKER_NM_UQ       = '有給休暇';
const TRACKER_NM_FURIQ    = '振替休暇';
const TRACKER_NM_DAIQ     = '代替休暇';
const TRACKER_NM_BIRTHDAY = '誕生日休暇';
const TRACKER_NM_BOOKS    = '書籍購入';
const TRACKER_NM_JUKEN    = '資格受験';
const TRACKER_NM_SEISAN   = '立替精算';
const TRACKER_NM_YOYAKU   = '会議室依頼';
const TRACKER_NM_KOUNYU   = '機器購入';

const ID_APP_DAYS = '#issue_custom_field_values_19';
// Redmineで２つの要素に同じIDを割り当てているバグがあるため、名前でアクセス
// const ID_NEGOTIATED = '#issue_custom_field_values_20';
const NM_NEGOTIATED = 'issue[custom_field_values][20]';

const NM_DELIVERY_ARRANGED = 'issue[custom_field_values][2]';
const NM_DELIVERY_DESTINATION = 'issue[custom_field_values][3]';
const NM_EXAM_PROCEDURE = 'issue[custom_field_values][22]';

const VAL_STATUS_NEW = '1';
const VAL_PRIORITY_NORMAL = '2';

const VAL_DELIVERY_ARRANGED_AFFAIRS = '総務手配';
const VAL_DELIVERY_DESTINATION_HOME = '自宅配送';
const VAL_EXAM_PROCEDURE_OWN = '各自手続き';
const VAL_NEGOTIATED = '1';


// 拡張機能で追加する識別子. Redmine側と衝突しないようにプレフィクスを付与すること
const APP_PREFIX = 'lazyApplicant_';

const ID_TRACKER_TEMPLATE_SELECT  = '#' + APP_PREFIX + 'tracker_select';
const ID_TRACKER_TEMPLATE_CHECK   = '#' + APP_PREFIX + 'tracker_check';
const ID_TRACKER_TEMPLATE_TEXT    = '#' + APP_PREFIX + 'tracker_text';

const NM_DESC_RADIO     　= APP_PREFIX + 'descriptionRadio';
const NM_APP_DAYS_RADIO 　= APP_PREFIX + 'applicantDaysRadio';
const NM_ASSIGNER_SELECT　= APP_PREFIX + 'usedAssignerselect';
const ID_ASSIGNER_SELECT  = '#' + APP_PREFIX + 'usedAssignerselect';

const ID_WATCHER_TEMPLATE_SELECT  = '#' + APP_PREFIX + 'watchers_select';
const ID_WATCHER_TEMPLATE_TEXT    = '#' + APP_PREFIX + 'watchers_text';

const ID_DIALOG_NODE = '#' + APP_PREFIX + 'dialog-node';

const NM_MACRO_FIELD_PREFIX = 'macro-' + APP_PREFIX;

const CLASS_DIALOG  = APP_PREFIX + 'dialog';
const CLASS_NO_WRAP = APP_PREFIX + 'no-wrap';
const CLASS_APPEND_CONTAINER = APP_PREFIX + 'append-container';
const CLASS_APPEND_BLOCK = APP_PREFIX + 'append-block';
const CLASS_APPEND_FIELD = APP_PREFIX + 'append-field';
const CLASS_TEMPLATE = APP_PREFIX + 'template';
const CLASS_TEMPLATE_SELECT = APP_PREFIX + 'template-select';
const CLASS_TEMPLATE_LABEL = APP_PREFIX + 'template-label';
const CLASS_TEMPLATE_TEXT = APP_PREFIX + 'template-text';
const CLASS_EXPAND_FEATURE = APP_PREFIX + 'expandFeature';
const CLASS_DISABLED_FEATURE = APP_PREFIX + 'disabledFeature';


// 拡張機能の内部で利用する定数
const ENABLE_GOOGLE_ANALYTICS = false; // GAによる統計収集機能を有効にするかどうか. trueにするとGA関連機能が動作します

const FEATURE_TYPE_TRACKER_TEMPLATE_ONLY = 'trackerTemplateOnly';
const FEATURE_TYPE_ALL = 'all';

const TEMPLATE_UPDATE_MASK_DEFAULT_SET = 1;
const TEMPLATE_UPDATE_MASK_DEFAULT_NON = 2;
const TEMPLATE_UPDATE_MASK_STORE = 4;

const LIST_SORT_TYPE_LAST_FREQUENCY = 1;
const LIST_SORT_TYPE_ONLY_FREQUENCY = 2;

const PORT_GA_CHANGE_AGREE = 'changeAgree';
const PORT_GA_PAGEVIEW = 'pageview';
const PORT_GA_TRACK_EVENT = 'event';

const GA_CATEGORY_ISSUE_ENTRY = 'issue-entry';
const GA_CATEGORY_OPTION_PAGE = 'option-page';

const URL_OPTION_PAGE = chrome.runtime.getURL('js/option-page/options.html');

const MACRO_TYPE_SYSTEM_DATE = 'sysdate';
const MACRO_SYSTEM_DATE_ymd_HYPHEN = '%Y-%m-%d';   // 2015-09-12 (yyyy-mm-dd)
const MACRO_SYSTEM_DATE_dmy_SLASH  = '%d/%m/%Y';   // 12/09/2015 (dd/mm/yyyy)
const MACRO_SYSTEM_DATE_dmy_DOT    = '%d.%m.%Y';   // 12.09.2015 (dd.mm.yyyy)
const MACRO_SYSTEM_DATE_dmy_HYPHEN = '%d-%m-%Y';   // 12-09-2015 (dd-mm-yyyy)
const MACRO_SYSTEM_DATE_mdy_SLASH  = '%m/%d/%Y';   // 09/12/2015 (mm/dd/yyyy)
const MACRO_SYSTEM_DATE_dby_SPACE  = '%d %b %Y';   // 12 9月 2015 (dd b yyyy)
const MACRO_SYSTEM_DATE_dBy_SPACE  = '%d %B %Y';   // 12 9月 2015 (dd B yyyy)
const MACRO_SYSTEM_DATE_bdy_COMMA  = '%b %d, %Y';  // 9月 12, 2015 (b dd, yyyy)
const MACRO_SYSTEM_DATE_Bdy_COMMA  = '%B %d, %Y';  // 9月 12, 2015 (B dd, yyyy)
const MACRO_SYSTEM_DATE_SET = [
    { format: MACRO_SYSTEM_DATE_ymd_HYPHEN, titleMessageId: 'content_date_ymd_HYPHEN' },
    { format: MACRO_SYSTEM_DATE_dmy_SLASH , titleMessageId: 'content_date_ymd_dmy_SLASH' },
    { format: MACRO_SYSTEM_DATE_dmy_DOT   , titleMessageId: 'content_date_ymd_dmy_DOT' },
    { format: MACRO_SYSTEM_DATE_dmy_HYPHEN, titleMessageId: 'content_date_ymd_dmy_HYPHEN' },
    { format: MACRO_SYSTEM_DATE_mdy_SLASH , titleMessageId: 'content_date_ymd_mdy_SLASH' },
    { format: MACRO_SYSTEM_DATE_dby_SPACE , titleMessageId: 'content_date_ymd_dby_SPACE' },
    { format: MACRO_SYSTEM_DATE_dBy_SPACE , titleMessageId: 'content_date_ymd_dBy_SPACE' },
    { format: MACRO_SYSTEM_DATE_bdy_COMMA , titleMessageId: 'content_date_ymd_bdy_COMMA' },
    { format: MACRO_SYSTEM_DATE_Bdy_COMMA , titleMessageId: 'content_date_ymd_Bdy_COMMA' }
];

const ARS_DEFAULT_TEMPLATE_SETTING_KEY = 'ars-workflow';
const ARS_DEFAULT_TEMPLATE_NAME = 'デフォルト';


// chrome extension quota
// use chrome.storage.sync
// ⇒https://developer.chrome.com/extensions/storage
function getChromeExtensionInfo() {
    const CHROME_EXT_SYNC_QUOTA_BYTES = 102400;
    const CHROME_EXT_SYNC_QUOTA_BYTES_PER_ITEM = 8192;
    const CHROME_EXT_SYNC_MAX_ITEMS = 512;

    const CHROME_EXT_LOCAL_QUOTA_BYTES = 5242880;
    const CHROME_EXT_LOCAL_QUOTA_BYTES_PER_ITEM = -1;
    const CHROME_EXT_LOCAL_MAX_ITEMS = -1;

    switch (CHROME_STORAGE.toLowerCase()) {
    case 'sync':
        return {
            isSync: true,
            storage: chrome.storage.sync,
            quotaBytes: CHROME_EXT_SYNC_QUOTA_BYTES,
            quotaBytesPerItem: CHROME_EXT_SYNC_QUOTA_BYTES_PER_ITEM,
            maxItems: CHROME_EXT_SYNC_MAX_ITEMS
        };
    case 'local':
        return {
            isSync: false,
            storage: chrome.storage.local,
            quotaBytes: CHROME_EXT_LOCAL_QUOTA_BYTES,
            quotaBytesPerItem: CHROME_EXT_LOCAL_QUOTA_BYTES_PER_ITEM,
            maxItems: CHROME_EXT_LOCAL_MAX_ITEMS
        };
    default:
        throw new Error('invalid CHROME_STORAGE. value=[' + CHROME_STORAGE + ']');
    }
}

/*eslint-enable no-multi-spaces, no-multiple-empty-lines, strict */
