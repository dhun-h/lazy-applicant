/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
lazyApp.Cscript = lazyApp.Cscript || {};
/*eslint-disable no-var */
!function() {
    'use strict';
    lazyApp.Cscript.PreSubmit = {

        do: function() {
            lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_ISSUE_ENTRY, 'チケット登録');
            storeExtensionFieldsInternal();
        },

        storeExtensionFields: function() {
            storeExtensionFieldsInternal();
        }
    };

    function storeExtensionFieldsInternal() {
        // setting
        var trackerId  = $(ID_TRACKER).val();
        var trackerTemplateName = $(ID_TRACKER_TEMPLATE_SELECT + ' option:selected').text();
        var watcherTemplateName = $(ID_WATCHER_TEMPLATE_SELECT + ' option:selected').text();
        var successBlock = null;

        var param = {
            trackerId: trackerId,
            trackerTemplateName: trackerTemplateName,
            usedAssignerId: $(ID_ASSIGNER).val(),
            watcherTemplateName: watcherTemplateName
        };
        lazyApp.DbFuncs.storeSettingByContentPage(
                lazyApp.Funcs.resolveSettingKey(), param, successBlock, lazyApp.UiFuncs.showErrorDialog);

        // temporary
        var dateMacros = {};
        var dateFields = $('.' + DATE_FIELD_CLASS);
        dateFields.each(function() {
            var id = NM_MACRO_FIELD_PREFIX + this.id;
            var dateMacro = $('#' + id);
            if (dateMacro.val()) {
                dateMacros[id] = dateMacro.val();
            }
        });

        param = {
            trackerTemplateSelect: trackerTemplateName,
            trackerTemplateCheck: $(ID_TRACKER_TEMPLATE_CHECK).prop('checked'),
            trackerTemplateText: lazyApp.UiFuncs.valForHintText(ID_TRACKER_TEMPLATE_TEXT),
            watcherTemplateSelect: watcherTemplateName,
            watcherTemplateText: lazyApp.UiFuncs.valForHintText(ID_WATCHER_TEMPLATE_TEXT),
            dateMacros: dateMacros
        };
        lazyApp.DbFuncs.storeTemporaryByContentPage(
                param, successBlock, lazyApp.UiFuncs.showErrorDialog);
    }
}();
