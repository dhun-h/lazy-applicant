/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
lazyApp.Cscript = lazyApp.Cscript || {};
/*eslint-disable no-var */
!function() {
    'use strict';

    var macroTodays = lazyApp.Funcs.provideTodays();

    lazyApp.Cscript.Date = {

        replaceNode: function(setting) {
            var dateFields = $('.' + DATE_FIELD_CLASS);

            dateFields.each(replaceDateNode);
        },

        setupDefault: function(appliedFields) {
            var dateFields = $('.' + DATE_FIELD_CLASS);
            dateFields.each(function() {
                var dateField = $(this);
                var id = this.id;
                var field = appliedFields[this.name];
                var macro = field ? field.macro : null;
                if (macro) {
                    macro = field ? field.value : null;
                } else {
                    var value = field ? field.value : null;
                    $.each(macroTodays, function(key, macroValue) {
                        if (value == macroValue) {
                            macro = key;
                            return false;
                        }
                    });
                }

                var select = $('#' + NM_MACRO_FIELD_PREFIX + id);
                select.val(macro);
                syncAttr(select, dateField);
            });
        },

        restoreRequestValue: function(temporary) {
            var dateFields = $('.' + DATE_FIELD_CLASS);
            dateFields.each(function() {
                var dateField = $(this);
                var id = NM_MACRO_FIELD_PREFIX + this.id;
                var select = $('#' + id);
                select.val(temporary.dateMacros[id]);
                syncAttr(select, dateField);
            });
        }
    };

    function replaceDateNode() {
        var dateField = $(this);
        var parent = dateField.parent();
        var isCustom = (-1 == $.inArray(dateField.prop('id'), ID_DATE_FIELD_SET));

        var select = $('<select>')
                .addClass(CLASS_APPEND_FIELD)
                .prop('id', NM_MACRO_FIELD_PREFIX + dateField.prop('id'))
                .prop('name', NM_MACRO_FIELD_PREFIX + dateField.prop('name'));

        select.append($('<option>').val(''));
        $.each(MACRO_SYSTEM_DATE_SET, function() {
            var title = chrome.i18n.getMessage(this.titleMessageId);
            var option = $('<option>')
                    .val(this.format)
                    .append(title);
            select.append(option);
        });
        select.addClass(isCustom ? 'lazyApplicant_custom-date-field' :
                                   'lazyApplicant_preset-date-field');

        var helpIcon = lazyApp.UiFuncs.createHelpIcon(
                '規定値/日付',
                chrome.i18n.getMessage('content_hint_date'))
                .attr('balloon-pos', 'bottom');
        lazyApp.UiFuncs.applyBalloon(helpIcon, {}, GA_CATEGORY_ISSUE_ENTRY);

        var group = $('<div>')
                .addClass(CLASS_APPEND_BLOCK)
                .append(select)
                .append(helpIcon);

        var container = $('<div>')
                .append(group)
                .addClass(CLASS_APPEND_CONTAINER);

        var originContainer = $('<div>')
                .css('float', 'left')
                .append(parent.children());

        parent.empty();
        parent.append(originContainer);
        parent.append(container);

        // event handler
        select.on('change', function() {
            var templateValue = syncAttr(select, dateField);
            dateField.val(templateValue);
        });

        dateField.on('keyup', function() {
            if (dateField.attr('templateValue') != dateField.val()) {
                select.val(null);
                syncAttr(select, dateField);
            }
        });

        var datePickerButton = $(originContainer.children()[2]);
        if (datePickerButton.exists() && datePickerButton.hasClass(DATE_PICKER_BUTTON_CLASS)) {
            var action = function() {
                select.val(null);
                syncAttr(select, dateField);

                // 再取得しないと正常に動作しない
                datePickerButton = $(originContainer.children()[2]);
                datePickerButton.on('click', action);
            };
            datePickerButton.on('click', action);
        }
    }

    function syncAttr(select, text) {
        var templateValue = null;
        var templateMacro = select.val();
        if (templateMacro) {
            templateValue = lazyApp.Funcs.provideToday(templateMacro);
        }
        text.attr('templateValue', templateValue);
        text.attr('templateMacro', templateMacro);

        return templateValue;
    }
}();
