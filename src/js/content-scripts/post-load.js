/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
lazyApp.Cscript = lazyApp.Cscript || {};
/*eslint-disable no-var */
!function() {
    'use strict';
    lazyApp.Cscript.PostLoad = function() {
        if (lazyApp.Funcs.isEntryPage() == false) {
            // チケット一覧では何もしない
            return;
        }

        var isTopPage = lazyApp.Funcs.isEntryTopPage();

        lazyApp.GaFuncs.pushGaPageViewedOnBackground(location.pathname);
        if (isTopPage) {
            lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_ISSUE_ENTRY, '初期表示');
        } else {
            lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_ISSUE_ENTRY, '検証エラー(redmine)');
        }

        // チケット登録 or チケット登録検証エラー
        var key = lazyApp.Funcs.resolveSettingKey();
        var trackerOptions = $(ID_TRACKER + ' option');
        var assignerOptions = $(ID_ASSIGNER + ' option');

        lazyApp.DbFuncs.findSettingByContentPage(
                key, trackerOptions, assignerOptions, function(globalSetting, setting) {
            // Redmine側の処理を上書き
            overrideUpdateIssueFrom(globalSetting, setting);

            // 要素追加
            appendDialogNode();
            lazyApp.Cscript.Watcher.replaceNode(setting);

            // 画面初期化
            var isSetupDefaults = isTopPage;
            var isFlushInitialFields = true;
            lazyApp.Cscript.IssueForm.setup(globalSetting, setting, isSetupDefaults, isFlushInitialFields);

            lazyApp.DbFuncs.storeGlobalSettingBySettingKey(key, function() {
                lazyApp.DbFuncs.storeSetting(key, setting, null, lazyApp.UiFuncs.showErrorDialog);
            }, lazyApp.UiFuncs.showErrorDialog);
        });
    };

    function appendDialogNode() {
        $('body').append($('<div>')
                .prop('id', ID_DIALOG_NODE.substr(1)));
    }

    function overrideUpdateIssueFrom(globalSetting, setting) {
        var isSetupDefaults;
        var onReloadDocument = function() {
            var isFlushInitialFields = false;
            lazyApp.Cscript.IssueForm.setup(globalSetting, null/*setting*/, isSetupDefaults, isFlushInitialFields);
        };

        var timeoutId= null;
        var changing = false;
        var onChange = function(e) {
            changing = true;
            isSetupDefaults = (e.target.id == ID_TRACKER.substr(1));

            // トラッカーを変更した場合
            if (isSetupDefaults) {

            // ステータスを変更された場合
            } else {
                // プラグインで追加した項目を復元できるように設定値を退避
                lazyApp.Cscript.PreSubmit.storeExtensionFields();
            }
        };

        $(ID_REPLACEMENT_ROOT).on('DOMNodeInserted', function(e) {
            if (changing == false) {
                return;
            }

            if (timeoutId) {
                clearTimeout(timeoutId);
            }
            timeoutId = setTimeout(function() {
                lazyApp.Funcs.logDebug('replaceIssueForm.');

                timeoutId= null;
                changing = false;

                onReloadDocument();
                $(ID_TRACKER).on('change', onChange);
                $(ID_STATUS ).on('change', onChange);
            }, 50);
        });
        $(ID_TRACKER).on('change', onChange);
        $(ID_STATUS ).on('change', onChange);
    }
}();
