/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
lazyApp.Cscript = lazyApp.Cscript || {};
/*eslint-disable no-var */
!function() {
    'use strict';

    // テンプレート適用完了のコールバック
    // テンプレート適用は非同期処理になるため、イベントを利用して同期化させている
    var trackerChangeCallback;

    // プロジェクトの初期表示値
    var projectInitialFields;

    // 適用したトラッカーテンプレート
    var applyTemplateFields;

    lazyApp.Cscript.Tracker = {

        replaceNode: function(setting) {
            var trackerId = $(ID_TRACKER).val();
            var tracker = setting.trackers[trackerId];

            var parent = $(ID_TRACKER).parent();

            var select = this.replaceSelectNode(tracker);
            var options = select.children();

            var check = $('<input type="checkbox">')
                    .val('1')
                    .attr('id', ID_TRACKER_TEMPLATE_CHECK.substr(1));

            var helpIconCheck = lazyApp.UiFuncs.createHelpIcon(
                    '規定値/トラッカーテンプレート',
                    chrome.i18n.getMessage('content_hint_useDefault', [
                        chrome.i18n.getMessage('content_hint_useDefault_trackerTemplate')
                    ]))
                    .attr('balloon-pos', 'bottom');
            lazyApp.UiFuncs.applyBalloon(helpIconCheck, {}, GA_CATEGORY_ISSUE_ENTRY);

            var label = $('<label>')
                    .addClass(CLASS_TEMPLATE_LABEL)
                    .append(check)
                    .append(chrome.i18n.getMessage('content_label_isDefault'));

            var text = $('<input type="text">')
                    .addClass(CLASS_APPEND_FIELD)
                    .addClass(CLASS_TEMPLATE_TEXT)
                    .attr('id', ID_TRACKER_TEMPLATE_TEXT.substr(1))
                    .attr('hint', chrome.i18n.getMessage('content_label_requireTemplateName'));

            var helpIconText = lazyApp.UiFuncs.createHelpIcon(
                    'トラッカーテンプレート'
                  , '<ul style="-webkit-padding-start:20px; margin:0;">' +
                    '<li>' + chrome.i18n.getMessage('content_hint_trackerTemplate01') +
                    '<ol>' +
                    '  <li>' + chrome.i18n.getMessage('content_hint_xxxxxxxTemplate02') + '</li>' +
                    '  <li>' + chrome.i18n.getMessage('content_hint_trackerTemplate03') + '</li>' +
                    '  <li>' + chrome.i18n.getMessage('content_hint_xxxxxxxTemplate04') + '</li>' +
                    '</ol>' +
                    chrome.i18n.getMessage('content_hint_trackerTemplateXX') +
                    '</li>' +
                    '<li>' + chrome.i18n.getMessage('content_hint_xxxxxxxTemplate05') + '</li>' +
                    '<li>' + chrome.i18n.getMessage('content_hint_xxxxxxxTemplate06') + '</li>' +
                    '</ul>')
                  .attr('balloon-pos', 'bottom');
            lazyApp.UiFuncs.applyBalloon(helpIconText, {
                offsetX: -200
            }, GA_CATEGORY_ISSUE_ENTRY);

            var storeIcon = lazyApp.UiFuncs.createStoreIcon(onClickStore);

            var optionIcon = lazyApp.UiFuncs.createOptionIcon(GA_CATEGORY_ISSUE_ENTRY, 'トラッカーテンプレート', setting.key);

            var group = $('<div>')
                    .addClass(CLASS_APPEND_BLOCK)
                    .addClass(CLASS_TEMPLATE)
                    .append(select)
                    .append(label)
                    .append(helpIconCheck)
                    .append(text)
                    .append(helpIconText)
                    .append(storeIcon)
                    .append(optionIcon);

            var container = $('<div>')
                    .append(group)
                    .addClass(CLASS_APPEND_CONTAINER);

            parent.css('overflow', 'initial');
            parent.append(container);

            // event handler
            select.on('change', function() {
                onSelect();
            });

            text.on('focus', lazyApp.UiFuncs.onFocusHintText);
            text.on('blur' , lazyApp.UiFuncs.onBluerHintText);
            text.on('keyup', function() {
                onChangeText(select, text, options);
            });
        },

        setTrackerChangeCallback: function(callback) {
            trackerChangeCallback = callback;
        },

        setupDefault: function(setting, isFlushInitialFields) {
            // テンプレート適用前に、初期表示時の設定値を保存
            updateProjectInitialFields(setting, isFlushInitialFields, function() {
                var trackerId = $(ID_TRACKER).val();
                var tracker = setting.trackers[trackerId];
                var defaultSetting = tracker.defaultTrackerTemplate;
                var name = defaultSetting.use ? defaultSetting.val : null;

                var select = $(ID_TRACKER_TEMPLATE_SELECT);
                select.val(name);
                onSelect(setting);
            });
        },

        restoreRequestValue: function(temporary) {
            var select = $(ID_TRACKER_TEMPLATE_SELECT);
            var check = $(ID_TRACKER_TEMPLATE_CHECK);
            var text = $(ID_TRACKER_TEMPLATE_TEXT);

            var template = temporary.trackerTemplate;
            var name = template.text ? template.text : template.select;
            select.val(name);

            check.prop('checked', template.check);

            text.val(template.text);
            text.trigger('blur');
        },

        replaceSelectNode: function(tracker) {
            var select = $(ID_TRACKER_TEMPLATE_SELECT);
            if (select.exists()) {
                select.empty();
            } else {
                select = $('<select>')
                        .attr('id', ID_TRACKER_TEMPLATE_SELECT.substr(1))
                        .addClass(CLASS_APPEND_FIELD)
                        .addClass(CLASS_TEMPLATE_SELECT);
            }

            var options = [];
            options.push($('<option>').val(''));
            $.each(tracker.templates, function() {
                options.push($('<option>')
                    .val(this.name)
                    .append(this.name));
            });
            $.each(options, function() {
                select.append(this);
            });

            return select;
        }
    };

    function updateProjectInitialFields(setting, isFlushInitialFields, callback) {
        var fields = extractFields(/*force=*/true);
        if (!(NM_WATCHER_CHECK in fields)) {
            // メンバが20名以上のプロジェクトでは、ウォッチャーはリスト選択形式になるため要素自体が存在しない
            fields[NM_WATCHER_CHECK] = {
                type: 'checkbox',
                value: ''
            };
        }
        lazyApp.DbFuncs.storeProjectInitialFields(setting, fields, isFlushInitialFields, function(newFields) {
            lazyApp.Cscript.Tracker.projectInitialFields = newFields;
            callback();
        }, lazyApp.UiFuncs.showErrorDialog);
    }

    function onSelect(setting) {
        var action = function(setting) {
            var trackerId = $(ID_TRACKER).val();
            var tracker = setting.trackers[trackerId];
            var templateName = $(ID_TRACKER_TEMPLATE_SELECT + ' option:selected').text();

            var check = $(ID_TRACKER_TEMPLATE_CHECK);
            if (tracker.defaultTrackerTemplate.use) {
                check.prop('checked', templateName == tracker.defaultTrackerTemplate.val);
            } else {
                check.prop('checked', !(templateName));
            }

            var text = $(ID_TRACKER_TEMPLATE_TEXT);
            text.val(templateName);
            text.trigger('blur');

            if (templateName) {
                var template = lazyApp.Funcs.extractTemplateByName(tracker.templates, templateName);
                lazyApp.DbFuncs.findTrackerTemplateFields(template.id, function(fields) {
                    restoreFields(setting, fields);
                });
            } else {
                restoreFields(setting, lazyApp.Cscript.Tracker.projectInitialFields);
            }
        };

        if (setting) {
            action(setting);
        } else {
            var key = lazyApp.Funcs.resolveSettingKey();
            lazyApp.DbFuncs.findSetting(key, action);
        }
    }

    function onChangeText(select, text, options) {
        var option  = options[0];

        $.each(options, function() {
            if (this.text == text.val()) {
                option = this;
                return false;
            }
        });
        select.val(option.value);
    }

    function onClickStore(e) {
        var key= lazyApp.Funcs.resolveSettingKey();
        var trackerId  = $(ID_TRACKER).val();
        var templateName = lazyApp.UiFuncs.valForHintText(ID_TRACKER_TEMPLATE_TEXT);
        var isDefault = $(ID_TRACKER_TEMPLATE_CHECK).prop('checked');

        var errMsg = '';
        if (!(templateName || isDefault)) {
            errMsg = chrome.i18n.getMessage('content_error_template_noopActionTracker');
        }
        if (errMsg) {
            lazyApp.UiFuncs.showDialog(ID_DIALOG_NODE, errMsg, 0);
            lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_ISSUE_ENTRY, '規定値更新/トラッカーテンプレート/エラー');
            return;
        }

        var param = {
            trackerId: trackerId,
            templateName: templateName,
            fields: extractFields(/*force=*/false),
            isDefault: isDefault
        };
        lazyApp.DbFuncs.storeTrackerTemplateByContentPage(key, param, function(newSetting) {

            var newTracker = newSetting.trackers[trackerId];

            // 保存したテンプレートを選択
            if (templateName) {
                var select = lazyApp.Cscript.Tracker.replaceSelectNode(newTracker);
                select.val(templateName);
            }

            // 保存した担当者を選択
            var assignerId = param.fields[NM_ASSIGNER] ? param.fields[NM_ASSIGNER].value : null;
            if (assignerId) {
                var select = lazyApp.Cscript.Assigner.replaceSelectNode(newSetting);
                select.val(assignerId);
            }

            // メッセージ
            var msg = '';
            var delm = '';
            if (newSetting.templateUpdateType & TEMPLATE_UPDATE_MASK_STORE) {
                msg += delm + chrome.i18n.getMessage('content_msg_storeTemplate', [templateName]);
                delm = '<br>';
            }
            if (newSetting.templateUpdateType & TEMPLATE_UPDATE_MASK_DEFAULT_SET) {
                msg += delm + chrome.i18n.getMessage('content_msg_updateDefault', [templateName]);
                delm = '<br>';
            }
            if (newSetting.templateUpdateType & TEMPLATE_UPDATE_MASK_DEFAULT_NON) {
                msg += delm + chrome.i18n.getMessage('content_msg_disableDefault');
                delm = '<br>';
            }
            lazyApp.UiFuncs.popupMessage(e.target, msg, {
                offsetX: -96,
                offsetY: -12,
                position: 'bottom',
                tipSize: 0,
                maxLifetime: 3000
            });

            lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_ISSUE_ENTRY, '規定値更新/トラッカーテンプレート');
        }, lazyApp.UiFuncs.showErrorDialog);
    }

    function extractFieldAttr(jObject) {
        var id = jObject.attr('id');
        var name = jObject.attr('name');
        var type = jObject.attr('type');
        if (type) {
            type = type.toLowerCase();
        } else {
            type = jObject.prop('tagName').toLowerCase();
        }

        if ((id == ID_TRACKER.substr(1)) ||
            (id == ID_STATUS.substr(1)) ||
            (id && id.startsWith(APP_PREFIX)) ||
            (name && name.startsWith(APP_PREFIX))) {
            return null;
        }

        switch (type) {
        case 'hidden':
        case 'file':
        case 'button':
        case 'submit':
        case 'reset':
            return null;

        default:
            // noop
        }

        return {
            id: id,
            name: name,
            type: type,
            macro: jObject.attr('macro')
        };
    }

    function extractFields(force) {
        var fields = {};

        $(ID_ISSUE_FORM + ' input,select,textarea').each(function() {
            var jObject = $(this);
            var attr = extractFieldAttr(jObject);
            if (attr == null) {
                return true;
            }

            var macro = '';
            var value = '';

            switch (attr.type) {
            case 'text':
            case 'textarea':
                if (jObject.hasClass(DATE_FIELD_CLASS)) {
                    var templateMacro = jObject.attr('templateMacro');
                    if (templateMacro) {
                        macro = MACRO_TYPE_SYSTEM_DATE;
                        value = templateMacro;
                    } else {
                        value = jObject.val();
                    }
                } else {
                    value = jObject.val();
                }
                break;

            case 'select':
                value = jObject.val();
                break;

            case 'checkbox':
            case 'radio':
                value = extractMultipleField(attr);
                break;

            default:
                var msg = 'extractFields unknown type! type=' + attr.type + ', name=' + attr.name;
                lazyApp.Funcs.logError(msg);
                lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_ISSUE_ENTRY, 'エラー/' + msg);
            }

            if (value || force) {
                fields[attr.name] = {
                    type: attr.type,
                    value: value
                };
                if (macro) {
                    fields[attr.name].macro = macro;
                }
            }
        });

        return fields;
    }

    function extractMultipleField(attr) {
        var values = [];
        $('input:' + attr.type + '[name="' + attr.name + '"]').each(function() {
            if (this.checked) {
                values.push(this.value);
            }
        });
        values.sort();
        return values.join(',');
    }

    function restoreFields(setting, fields) {
        // 適用したテンプレートフィールドを保持
        lazyApp.Cscript.Tracker.applyTemplateFields = fields;

        // メンバが20名以上のプロジェクトでは、ウォッチャーはリスト選択形式になるため要素自体が存在しない
        var fieldSetting = fields[NM_WATCHER_CHECK];
        var values = fieldSetting ? fieldSetting.value.split(',') : [];
        $.each(values, function() {
            var check = $('input:checkbox[name="' + NM_WATCHER_CHECK + '"][value="' + this + '"]');
            if (check.exists() == false) {
                var assigner = setting.assigners[this];
                lazyApp.UiFuncs.appendWatcherCheckbox(assigner);
            }
        });

        $(ID_ISSUE_FORM + ' input,select,textarea').each(function() {
            var jObject = $(this);
            var attr = extractFieldAttr(jObject);
            if (attr == null) {
                return true;
            }

            var templateField = lazyApp.Cscript.Tracker.applyTemplateFields[attr.name];
            var templateMacro = templateField ? templateField.macro : '';
            var templateValue = templateField ? templateField.value : '';

            switch (attr.type) {
            case 'text':
            case 'textarea':
                switch (templateMacro) {
                case MACRO_TYPE_SYSTEM_DATE:
                    templateValue = lazyApp.Funcs.provideToday(templateValue);
                    break;
                default:
                    // noop
                }
                jObject.val(templateValue);
                break;

            case 'select':
                jObject.val(templateValue);
                break;

            case 'checkbox':
            case 'radio':
                var values = templateValue ? templateValue.split(',') : [];
                $('input:' + attr.type + '[name="' + attr.name + '"]').each(function() {
                    this.checked = (-1 < $.inArray(this.value, values));
                });
                break;

            default:
                var msg = 'restoreFields unknown type! type=' + attr.type + ', name=' + attr.name;
                lazyApp.Funcs.logError(msg);
                lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_ISSUE_ENTRY, 'エラー/' + msg);
            }
        });

        // テンプレート適用完了のコールバックを実行
        trackerChangeCallback(fields);
    }
}();
