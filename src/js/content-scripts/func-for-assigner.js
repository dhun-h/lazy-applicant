/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
lazyApp.Cscript = lazyApp.Cscript || {};
/*eslint-disable no-var */
!function() {
    'use strict';
    lazyApp.Cscript.Assigner = {

        replaceNode: function(setting) {
            var parent = $(ID_ASSIGNER).parent();
            var child1 = parent.children()[0];
            var child2 = parent.children()[1];

            var selectOrg = $('#' + child2.id);
            var selectNew = this.replaceSelectNode(setting);

            var helpIconUsedAssigner = lazyApp.UiFuncs.createHelpIcon(
                    '利用した担当者'
                  , '<ul style="-webkit-padding-start:20px; margin:0;">' +
                    '<li>' + chrome.i18n.getMessage('content_hint_usedAssigner01') +
                    '<ul>' +
                    '  <li>' + chrome.i18n.getMessage('content_hint_usedAssigner02') + '</li>' +
                    '  <li>' + chrome.i18n.getMessage('content_hint_usedAssigner03') + '</li>' +
                    '</ul>' +
                    '</li>' +
                    '<li>' + chrome.i18n.getMessage('content_hint_usedAssigner04') + '</li>' +
                    '</ul>')
                    .attr('balloon-pos', 'bottom');
            lazyApp.UiFuncs.applyBalloon(helpIconUsedAssigner, {}, GA_CATEGORY_ISSUE_ENTRY);

            var optionIcon = lazyApp.UiFuncs.createOptionIcon(GA_CATEGORY_ISSUE_ENTRY, '担当者', setting.key);

            var group = $('<div>')
                    .addClass(CLASS_APPEND_BLOCK)
                    .addClass(CLASS_EXPAND_FEATURE)
                    .append(selectNew)
                    .append(helpIconUsedAssigner)
                    .append(optionIcon);

            var container = $('<div>')
                    .append(group)
                    .addClass(CLASS_APPEND_CONTAINER)
                    .addClass(CLASS_DISABLED_FEATURE);

            parent.append(container);

            // event handler
            selectOrg.on('change', function() {
                sync(selectOrg);
            });
            selectNew.on('change', function() {
                sync(selectNew);
            });
        },

        sync: function() {
            sync($(ID_ASSIGNER));
        },

        replaceSelectNode: function(setting) {
            var select = $(ID_ASSIGNER_SELECT);
            if (select.exists()) {
                select.empty();
            } else {
                select = $('<select>')
                    .attr('id', ID_ASSIGNER_SELECT.substr(1))
                    .addClass(CLASS_APPEND_FIELD);
            }

            select.append($('<option>').val(''));
            $.each(setting.usedAssigners, function() {
                var assigner = setting.assigners[this.id];
                select.append($('<option>').val(this.id).append(assigner.name));
            });

            return select;
        }
    };

    function sync(changedSelect) {
        var assigners = $(ID_ASSIGNER);
        var usedAssigners = $(ID_ASSIGNER_SELECT);
        var syncSelect = (changedSelect.get(0) != assigners.get(0) ? assigners : usedAssigners);

        syncSelect.val(changedSelect.val());
    }
}();
