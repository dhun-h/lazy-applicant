/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
lazyApp.Cscript = lazyApp.Cscript || {};
/*eslint-disable no-var */
!function() {
    'use strict';
    lazyApp.Cscript.Description = {

        replaceNode: function() {
            var parent = $(ID_DESC).parent().parent();
            var child1 = parent.children()[0];
            var child2 = parent.children()[1];

            var makeRadio = function(title, value) {
                var radio = $('<input type="radio">')
                        .val(value != null ? value : title)
                        .attr('name', NM_DESC_RADIO)
                        .on('click', function() {
                            onClick(radio);
                        });

                return $('<label class="block">')
                        .append(radio)
                        .append(title);
            };

            parent.empty();
            parent.append(makeRadio('私用のため'));
            parent.append(makeRadio('体調不良のため'));
            parent.append(makeRadio('その他', ''));
            parent.append(child1);
            parent.append(child2);

            // event handler
            var radios = [];
            $('input[name="' + NM_DESC_RADIO + '"]').each(function() {
                radios.push($(this));
            });

            $(ID_DESC).on('keyup', function() {
                var text = $(ID_DESC);
                sync(text, radios);
            });
        },

        sync: function() {
            $(ID_DESC).trigger('keyup');
        }
    };

    function　onClick(radio) {
        $(ID_DESC).val(radio.val());
    }

    function sync(text, radios) {
        // var text = $(ID_DESC);
        // var radios = $('input[name="' + NM_DESC_RADIO + '"]');
        var radio  = radios[2];
        $.each(radios, function() {
            if (this.val() == text.val()) radio = this;
        });
        radio.prop('checked', true);
    }
}();
