/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
lazyApp.Cscript = lazyApp.Cscript || {};
/*eslint-disable no-var */
!function() {
    'use strict';
    lazyApp.Cscript.IssueForm = {

        setup: function(globalSetting, setting, isSetupDefaults, isFlushInitialFields) {
            if (setting) {
                setupInternal(globalSetting, setting, isSetupDefaults, isFlushInitialFields);
            } else {
                var key = lazyApp.Funcs.resolveSettingKey();
                lazyApp.DbFuncs.findSetting(key, function(setting) {
                    setupInternal(globalSetting, setting, isSetupDefaults, isFlushInitialFields);
                });
            }
        }
    };

    function setupInternal(globalSetting, setting, isSetupDefaults, isFlushInitialFields) {
        var trackerId = $(ID_TRACKER).val();
        var tracker = setting.trackers[trackerId];

        lazyApp.Funcs.logDebug('IssueForm#setup ---');
        lazyApp.Funcs.logDebug(tracker);

        // 要素追加
        lazyApp.Cscript.Tracker.replaceNode(setting);
        lazyApp.Cscript.Assigner.replaceNode(setting);
        lazyApp.Cscript.Date.replaceNode(setting);

        // 玄人向け要素の表示切り替え
        var expandFeatures = $('.' + CLASS_EXPAND_FEATURE).parent();
        if (globalSetting.featureType == FEATURE_TYPE_TRACKER_TEMPLATE_ONLY) {
            expandFeatures.addClass(CLASS_DISABLED_FEATURE);
        } else {
            expandFeatures.removeClass(CLASS_DISABLED_FEATURE);
        }

        // 拡張機能で追加した部分を強調表示
        var appendBlocks = $('.' + CLASS_APPEND_BLOCK
                + ':not(.' + CLASS_DISABLED_FEATURE + ')');
        appendBlocks.effect('bounce', {
            'direction': 'left',
            'distance': 40,
            'mode': 'effect'
        }, 500);

        // ARS社内申請向けの要素追加
        if (lazyApp.Funcs.isArsWorkflowPage()) {
            switch (trackerId) {
            case TRACKER_ID_UQ:
                lazyApp.Cscript.Description.replaceNode();
                /* FALLTHROUGH */

            case TRACKER_ID_FURIQ:
            case TRACKER_ID_DAIQ:
                lazyApp.Cscript.ApplicantDays.replaceNode();
                break;

            case TRACKER_ID_BIRTHDAY:
                break;

            default:
            }
        }

        // 規定値設定

        // トラッカーが変更された時に、最後に呼び出される処理
        // ストレージからのデータ取得が非同期処理のため、まわりくどい実装になった
        //   ・lazyApp.Cscript.Tracker.setupDefaultの事後処理
        //   ・ユーザがトラッカーを変更した時の事後処理
        lazyApp.Cscript.Tracker.setTrackerChangeCallback(function(appliedFields) {
            lazyApp.Cscript.Watcher.setupDefault(setting);
            lazyApp.Cscript.Date.setupDefault(appliedFields);

            lazyApp.Cscript.Description.sync();
            lazyApp.Cscript.Assigner.sync();
            lazyApp.Cscript.ApplicantDays.sync();
        });

        if (isSetupDefaults) {
            lazyApp.Cscript.Tracker.setupDefault(setting, isFlushInitialFields);
        } else {
            lazyApp.DbFuncs.findTemporary(function(temporary) {
                lazyApp.Cscript.Tracker.restoreRequestValue(temporary);
                lazyApp.Cscript.Watcher.restoreRequestValue(temporary);
                lazyApp.Cscript.Date.restoreRequestValue(temporary);

                lazyApp.Cscript.Description.sync();
                lazyApp.Cscript.Assigner.sync();
                lazyApp.Cscript.ApplicantDays.sync();
            });
        }
    }
}();
