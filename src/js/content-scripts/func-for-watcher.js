/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
lazyApp.Cscript = lazyApp.Cscript || {};
/*eslint-disable no-var */
!function() {
    'use strict';
    lazyApp.Cscript.Watcher = {

        replaceNode: function(setting) {
            var parent = $(ID_WATCHERS_FORM);
            var child1 = parent.children()[0];
            var child2 = parent.children()[1];
            var child3 = parent.children()[2];

            var select = this.replaceSelectNode(setting);
            var options = select.children();

            var text = $('<input type="text">')
                    .addClass(CLASS_APPEND_FIELD)
                    .addClass(CLASS_TEMPLATE_TEXT)
                    .attr('id', ID_WATCHER_TEMPLATE_TEXT.substr(1))
                    .attr('hint', chrome.i18n.getMessage('content_label_requireTemplateName'));

            var helpIconText = lazyApp.UiFuncs.createHelpIcon(
                    'ウォッチャーテンプレート'
                  , '<ul style="-webkit-padding-start:20px; margin:0;">' +
                    '<li>' + chrome.i18n.getMessage('content_hint_watcherTemplate01') +
                    '<ol>' +
                    '  <li>' + chrome.i18n.getMessage('content_hint_xxxxxxxTemplate02') + '</li>' +
                    '  <li>' + chrome.i18n.getMessage('content_hint_watcherTemplate03') + '</li>' +
                    '  <li>' + chrome.i18n.getMessage('content_hint_xxxxxxxTemplate04') + '</li>' +
                    '</ol>' +
                    '</li>' +
                    '<li>' + chrome.i18n.getMessage('content_hint_xxxxxxxTemplate05') + '</li>' +
                    '<li>' + chrome.i18n.getMessage('content_hint_xxxxxxxTemplate06') + '</li>' +
                    '</ul>')
                    .attr('balloon-pos', 'top');
            lazyApp.UiFuncs.applyBalloon(helpIconText, {
                offsetX: -200
            }, GA_CATEGORY_ISSUE_ENTRY);

            var storeIcon = lazyApp.UiFuncs.createStoreIcon(onClickStore);

            var optionIcon = lazyApp.UiFuncs.createOptionIcon(GA_CATEGORY_ISSUE_ENTRY, 'ウォッチャーテンプレート', setting.key);

            var group = $('<div>')
                    .addClass(CLASS_APPEND_BLOCK)
                    .addClass(CLASS_TEMPLATE)
                    .addClass(CLASS_EXPAND_FEATURE)
                    .append(select)
                    .append(text)
                    .append(helpIconText)
                    .append(storeIcon)
                    .append(optionIcon);

            var container = $('<div>')
                    .append(group)
                    .addClass(CLASS_APPEND_CONTAINER)
                    .addClass(CLASS_DISABLED_FEATURE);

            parent.empty();
            parent.append(child1);
            parent.append(container);
            parent.append(child2);
            parent.append(child3);

            // event handler
            select.on('change', function() {
                onSelect();
            });

            text.on('focus', lazyApp.UiFuncs.onFocusHintText);
            text.on('blur' , lazyApp.UiFuncs.onBluerHintText);
            text.on('keyup', function() {
                onChangeText(select, text, options);
            });
        },

        setupDefault: function(setting) {
            var select = $(ID_WATCHER_TEMPLATE_SELECT);
            select.val(null);

            var text = $(ID_WATCHER_TEMPLATE_TEXT);
            text.val(null);
            text.trigger('blur');
        },

        restoreRequestValue: function(temporary) {
            var select = $(ID_WATCHER_TEMPLATE_SELECT);
            var text = $(ID_WATCHER_TEMPLATE_TEXT);

            var template = temporary.watcherTemplate;
            var name = template.text ? template.text : template.select;
            select.val(name);

            text.val(template.text);
            text.trigger('blur');
        },

        replaceSelectNode: function(setting) {
            var select = $(ID_WATCHER_TEMPLATE_SELECT);
            if (select.exists()) {
                select.empty();
            } else {
                select = $('<select>')
                        .attr('id', ID_WATCHER_TEMPLATE_SELECT.substr(1))
                        .addClass(CLASS_APPEND_FIELD)
                        .addClass(CLASS_TEMPLATE_SELECT);
            }

            var options = [];
            options.push($('<option>').val(''));
            $.each(setting.watcherTemplates, function() {
                options.push($('<option>')
                    .prop('idList', this.ids)
                    .val(this.name)
                    .append(this.name));
            });
            $.each(options, function() {
                select.append(this);
            });

            return select;
        }
    };

    function onSelect() {
        var key = lazyApp.Funcs.resolveSettingKey();
        lazyApp.DbFuncs.findSetting(key, function(setting) {
            var templateName = $(ID_WATCHER_TEMPLATE_SELECT + ' option:selected').text();
            var template = lazyApp.Funcs.extractTemplateByName(setting.watcherTemplates, templateName);
            if (template == null) {
                template = {
                    ids: []
                };
            }
            restoreCheckedWatcher(setting, template);

            var text = $(ID_WATCHER_TEMPLATE_TEXT);
            text.val(templateName);
            text.trigger('blur');
        });
    }

    function onChangeText(select, text, options) {
        var option  = options[0];

        $.each(options, function() {
            if (this.text == text.val()) {
                option = this;
                return false;
            }
        });
        select.val(option.value);
    }

    function onClickStore(e) {
        var key= lazyApp.Funcs.resolveSettingKey();
        var trackerId  = $(ID_TRACKER).val();
        var storeTemplateName = lazyApp.UiFuncs.valForHintText(ID_WATCHER_TEMPLATE_TEXT);
        var defaultTemplateName = storeTemplateName || $(ID_WATCHER_TEMPLATE_SELECT + ' option:selected').text();
        var watcherIds = extractCheckedWatcher();
        var errMsg = '';
        if (!(storeTemplateName)) {
            errMsg = chrome.i18n.getMessage('content_error_template_noopActionWatcher');
        }
        if (errMsg) {
            lazyApp.UiFuncs.showDialog(ID_DIALOG_NODE, errMsg, 0);
            lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_ISSUE_ENTRY, '規定値更新/ウォッチャーテンプレート/エラー');
            return;
        }

        var param = {
            trackerId: trackerId,
            storeTemplateName: storeTemplateName,
            watcherIds: watcherIds,
            defaultTemplateName: defaultTemplateName
        };
        lazyApp.DbFuncs.storeWatcherTemplateByContentPage(key, param, function(newSetting) {

            // 保存したテンプレートを選択
            if (storeTemplateName) {
                var select = lazyApp.Cscript.Watcher.replaceSelectNode(newSetting);
                select.val(storeTemplateName);
            }

            var msg = '';
            if (storeTemplateName) {
                msg += chrome.i18n.getMessage('content_msg_storeTemplate', [storeTemplateName]);
            }
            lazyApp.UiFuncs.popupMessage(e.target, msg, {
                offsetX: -96,
                offsetY: +12,
                position: 'top',
                tipSize: 0,
                maxLifetime: 3000
            });

            lazyApp.GaFuncs.pushGaTrackEventOnBackground(GA_CATEGORY_ISSUE_ENTRY, '規定値更新/ウォッチャーテンプレート');
        }, lazyApp.UiFuncs.showErrorDialog);
    }

    function extractCheckedWatcher() {
        var values = [];
        var checks = $('input:checkbox[name="' + NM_WATCHER_CHECK + '"]:checked');
        checks.each(function() {
            values.push(this.value);
        });
        values.sort();

        return values;
    }

    function restoreCheckedWatcher(setting, template) {
        // メンバが20名以上のプロジェクトでは、ウォッチャーはリスト選択形式になるため要素自体が存在しない
        $.each(template.ids, function() {
            var check = $('input:checkbox[name="' + NM_WATCHER_CHECK + '"][value="' + this + '"]');
            if (check.exists() == false) {
                var assigner = setting.assigners[this];
                lazyApp.UiFuncs.appendWatcherCheckbox(assigner);
            }
        });

        $('input:checkbox[name="' + NM_WATCHER_CHECK + '"]').each(function() {
            this.checked = (-1 < $.inArray(this.value, template.ids));
        });
    }
}();
