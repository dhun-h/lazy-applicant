/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

!function() {
    'use strict';

    lazyApp.UiFuncs.prepareBalloonDefaults();

    lazyApp.Cscript.PostLoad();
    document.addEventListener('submit', lazyApp.Cscript.PreSubmit.do, false);
}();
