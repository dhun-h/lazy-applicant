/**
 * Copyright 2015 dhun
 *
 * http://choosealicense.com/no-license/
 */

/*eslint-disable no-var */
var lazyApp = lazyApp || {};
lazyApp.Cscript = lazyApp.Cscript || {};
/*eslint-disable no-var */
!function() {
    'use strict';
    lazyApp.Cscript.ApplicantDays = {

        replaceNode: function() {
            var parent = $(ID_APP_DAYS).parent();
            var child1 = parent.children()[0];
            var child2 = parent.children()[1];

            var makeRadio = function(value, title, addClass) {
                var radio = $('<input type="radio">')
                        .val(value)
                        .attr('name', NM_APP_DAYS_RADIO)
                        .on('click', function() {
                            onClick(radio);
                        });

                addClass = (addClass ? ' ' + addClass : '');
                return $('<label class="block' + addClass + '">')
                        .append(radio)
                        .append(title);
            };

            parent.empty();
            parent.append(child1);
            parent.append(makeRadio('1.0', '全休', CLASS_NO_WRAP));
            parent.append(makeRadio('0.5', '半休', CLASS_NO_WRAP));
            parent.append(makeRadio('0.0', 'その他'));
            parent.append(child2);

            // event handler
            $(ID_APP_DAYS).on('keyup', sync);
        },

        sync: function() {
            $(ID_APP_DAYS).trigger('keyup');
        }
    };

    function onClick(radio) {
        if (radio.val() > 0) {
            $(ID_APP_DAYS).val(radio.val());
        } else {
            $(ID_APP_DAYS).val(null);
        }
    }

    function sync() {
        var text = $(ID_APP_DAYS);
        var radios = $('input[name="' + NM_APP_DAYS_RADIO + '"]');
        var radio  = $(radios[2]);
        radios.each(function() {
            var cur = $(this);
            if (Number(cur.val()) == Number(text.val())) radio = cur;
        });
        radio.prop('checked', true);
    }
}();
