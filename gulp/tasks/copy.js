var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var config = require('../config').copy;


gulp.task('copy', function(callback) {
    runSequence(
        ['copy-src', 'copy-lib', 'copy-license'],
        callback
    );
});

// src without lib
gulp.task('copy-src', function() {
    return gulp.src([config.base.sourceDir + '**/*'
              ,'!' + config.base.sourceDir + '**/*-sample'
              ,'!' + config.base.sourceDir + 'lib/**'])
        .pipe($.if('*.js', $.uglify(config.uglify).on('error', $.util.log)))
        .pipe($.if('*.css', $.minifyCss(config.minifyCss).on('error', $.util.log)))
        .pipe($.if('*.png', $.cache($.imagemin(config.imagemin).on('error', $.util.log))))
        .pipe(gulp.dest(config.base.versionDir));
});

// lib
gulp.task('copy-lib', function() {
    return gulp.src(config.base.sourceDir + 'lib/**/*')
        .pipe(gulp.dest(config.base.versionDir + '/lib'));
});

// license
gulp.task('copy-license', function() {
    return gulp.src(config.base.sourceDir + '../license.txt')
        .pipe(gulp.dest(config.base.versionDir + '/'));
});
