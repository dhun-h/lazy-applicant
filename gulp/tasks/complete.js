var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var config = require('../config').simple;

gulp.task('complete', function() {
    gulp.src([config.versionCrxFile + '*'
             ,config.latestCrxFile  + '*'
             ,config.versionZipFile + '*'])
        .pipe($.notify({
            title: 'Lazy Applicant',
            message: 'build: <%= file.relative %>'
        }));
});
