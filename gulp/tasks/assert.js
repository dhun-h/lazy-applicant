var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var fs = require('fs');
var config = require('../config').simple;

gulp.task('assert', function() {
    var abort = function(msg) {
        $.util.log($.util.colors.red(msg));
        process.exit(9);
    };
    var validateExists = function(file) {
        if (!fs.existsSync(file)) {
            abort('ファイルが見つかりません. [' + file + ']');
        }
    };

    switch (config.env.profile) {
    case 'release':
    case 'develop':
        break;
    default:
        abort('gulp/config-env.jsのプロファイルが不正です. profile=[' + config.env.profile + ']');
    }
    if (!config.version) {
        abort('manifest.jsonからバージョン番号を取得できません.');
    }

    validateExists('gulp/config-env.js');
    validateExists(config.keyFile);
    validateExists(config.env.chrome.replace(/\\ /g, ' '));
    validateExists(config.sourceDir + 'js/env.js');
    validateExists(config.sourceDir + 'js/env-release.js');
});
