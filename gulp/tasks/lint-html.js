/**
 * 機能１：htmlファイルをLintする
 * 機能２：htmlファイルを監視して、変更があればLintする
 *
 * usage：すべてのhtmlファイルをLint
 *           $ gulp lint-html
 *
 * usage：環境変数 LINT_HTML_TARGET で指定したhtmlファイルをLint
 *           $ export LINT_HTML_TARGET=src/manifest.json
 *           $ gulp lint-html
 *
 * usage：すべてのhtmlファイルを監視して、変更があればLint
 *           $ gulp lint-html-watch
 *
 * usage：環境変数 LINT_HTML_TARGET で指定したhtmlファイルを監視して、変更があればLint
 *           $ export LINT_HTML_TARGET=src/manifest.json
 *           $ gulp lint-html-watch
 */
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');

function provideTargets(dest) {
    var target = process.env.LINT_HTML_TARGET || dest;
    return Array.isArray(target) ? target : target.split(',');
}

gulp.task('lint-html', function() {
    var config = require('../config').lint_html;
    var targets = provideTargets(config.dest);

    return gulp.src(targets)
        .pipe($.htmlhint('.htmlhintrc'))
        .pipe($.htmlhint.failReporter());
        // .pipe($.htmlValidator(function(file) {
        //     format: 'json'
        // }))
        //
        // .pipe($.intercept(function(file) {
        //     var json = JSON.parse(file.contents.toString());
        //     var errors = json.messages.filter(function(e, i, a) {
        //         return e.type != 'info'
        //     });
        //     if (errors.length == 0) {
        //         console.info('HTML OK! file=[' + file + ']');
        //     } else {
        //         console.error('\u001b[31m HTML ERROR! file=[' + file + ']');
        //         console.error(errors);
        //         console.error('\u001b[0m\n' + "\n");
        //     }
        // }));
});

gulp.task('lint-html-watch', function() {
    var config = require('../config').lint_html_watch;
    var targets = provideTargets(config.dest);
    console.info('targets=' + targets);

    gulp.watch(targets, ['lint-html']);
});
