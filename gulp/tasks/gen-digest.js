var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var merge = require('event-stream').merge;
var config = require('../config').simple;

gulp.task('gen-digest', function(callback) {
    return merge(
        gulp.src(config.versionCrxFile)
            .pipe($.hashsum({
                filename: config.versionCrxFile + '.md5'
              , hash: 'md5'
        }))
      , gulp.src(config.latestCrxFile)
            .pipe($.hashsum({
                filename: config.latestCrxFile + '.md5'
              , hash: 'md5'
        }))
    );
});
