var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('build', function(callback) {
    runSequence(
        'assert'
      ,['lint-js', 'lint-html']
      , 'clean'
      , 'copy'
      , 'switch-env'
      , 'package'
      ,['build-sub-crx', 'build-sub-zip']
      , 'complete'
      , callback
    );
});

gulp.task('build-sub-crx', function(callback) {
    runSequence(
        'package'
      , 'move-crx'
      , 'gen-latest'
      , 'gen-digest'
      , callback
    );
});

gulp.task('build-sub-zip', function(callback) {
    runSequence(
        'gen-zip'
      , callback
    );
});
