var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var config = require('../config').simple;

gulp.task('gen-zip', function () {
    return gulp.src(config.versionDir + '/**')
        .pipe($.zip(config.versionZipName))
        .pipe(gulp.dest(config.zipDir));
});
