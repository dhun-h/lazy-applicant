var gulp = require('gulp');
var del = require('del');
var config = require('../config').simple;

gulp.task('move-crx', function() {
    return gulp.src(config.createdCrxFile)
        .pipe(gulp.dest(config.crxDir))

        .on('end', function() {
            del.sync(config.createdCrxFile);
        });
});
