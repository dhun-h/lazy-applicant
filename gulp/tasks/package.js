var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var config = require('../config').simple;

gulp.task('package', $.shell.task([
    config.env.chrome
            + ' --pack-extension=' + config.versionDir
            + ' --pack-extension-key=' + config.keyFile
]));
