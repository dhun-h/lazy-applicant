var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var del = require('del');
var runSequence = require('run-sequence');
var config = require('../config').simple;

gulp.task('switch-env', function(callback) {
    if (config.env.profile == 'release') {
        runSequence('switch-release');
    } else {
        runSequence('switch-develop');
    }
    callback();
});

gulp.task('switch-release', function(callback) {
    gulp.src(config.versionDir + '/**/*-release.js')
        .pipe($.if(/env-release.js$/, $.rename('js/env.js')))
        .pipe(gulp.dest(config.versionDir))

        .on('end', function() {
            del.sync(config.versionDir + '/**/*-release.js');
            callback();
        });
});

gulp.task('switch-develop', function(callback) {
    del.sync(config.versionDir + '/**/*-release.js');
    callback();
});
