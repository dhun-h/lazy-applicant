/**
 * 機能１：jsファイルをLintする
 * 機能２：jsファイルを監視して、変更があればLintする
 *
 * usage：すべてのjsファイルをLint
 *           $ gulp lint-js
 *
 * usage：環境変数 LINT_JS_TARGET で指定したjsファイルをLint
 *           $ export LINT_JS_TARGET=src/manifest.json
 *           $ gulp lint-js
 *
 * usage：すべてのjsファイルを監視して、変更があればLint
 *           $ gulp lint-js-watch
 *
 * usage：環境変数 LINT_JS_TARGET で指定したjsファイルを監視して、変更があればLint
 *           $ export LINT_JS_TARGET=src/js/background/background.js
 *           $ gulp lint-js-watch
 */
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');

function provideTargets(dest) {
    var target = process.env.LINT_JS_TARGET || dest;
    return Array.isArray(target) ? target : target.split(',');
}

gulp.task('lint-js', function() {
    var config = require('../config').lint_js;
    var targets = provideTargets(config.dest);

    return gulp.src(targets)
        .pipe($.eslint({useEslintrc: true}))
        .pipe($.eslint.format())
        .pipe($.eslint.failAfterError());
});

gulp.task('lint-js-watch', function() {
    var config = require('../config').lint_js_watch;
    var targets = provideTargets(config.dest);
    console.info('targets=' + targets);

    gulp.watch(targets, ['lint-js']);
});
