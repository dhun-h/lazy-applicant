var gulp = require('gulp');
var del = require('del');
var config = require('../config').simple;

gulp.task('clean', function() {
    del.sync([config.versionDir
            , config.versionCrxFile + '*'
            , config.latestCrxFile + '*']);
});
