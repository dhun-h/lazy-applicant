var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var config = require('../config').simple;

gulp.task('gen-latest', function() {
    return gulp.src(config.versionCrxFile)
        .pipe($.rename(config.latestCrxName))
        .pipe(gulp.dest(config.crxDir));
});
