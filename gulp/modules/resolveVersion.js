var fs = require('fs');

module.exports = function() {
    var content = fs.readFileSync('src/manifest.json', 'utf8');
    var match = content.match(/"version": *"([\d\.]+)"/);
    if (match && match.length == 2) {
        return match[1];
    }

    return null;
}();
