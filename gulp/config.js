var configEnv = require('./config-env.js');
var version = require('./modules/resolveVersion');

var publishDir = 'publish/';
var sourceDir = 'src/';
var crxDir = publishDir + 'crx/';
var zipDir = publishDir + 'zip/';
var namePrefix = configEnv.profile == 'release' ? '' : 'dev-';
var versionName = namePrefix + 'lazy-applicant-' + version;
var latestCrxName  = namePrefix + 'lazy-applicant-latest.crx';

var baseConfig = {
    env: configEnv
  , version: version
  , sourceDir: sourceDir
  , publishDir: publishDir
  , versionDir: publishDir + 'build/' + versionName + '/'

  , crxDir: crxDir
  , zipDir: zipDir

  , keyFile:        publishDir + 'lazy-applicant.pem'
  , createdCrxFile: publishDir + 'build/' + versionName + '.crx'
  , versionCrxFile: crxDir + versionName + '.crx'
  , latestCrxFile:  crxDir + latestCrxName
  , latestCrxName:  latestCrxName

  , versionZipFile: zipDir + versionName + '.zip'
  , versionZipName: versionName + '.zip'
  };

module.exports = {
    simple: baseConfig,

    copy: {
        base: baseConfig

      , uglify: {}
      , minifyCss: {}
      , minifyHTML: {
            quotes: true
          , loose: true
      , }
      , imagemin: {
            optimizationLevel: 5
          , progressive: true
          , interlaced: true
        }
    },

    lint_js: {
        dest: [
            sourceDir + 'js/**/*.js'
        ]
    },

    lint_js_watch: {
        dest: [
            sourceDir + 'js/**/*.js'
          , '.eslintrc'
        ]
    },

    lint_html: {
        dest: [
            sourceDir + 'js/**/*.html'
        ]
    },

    lint_html_watch: {
        dest: [
            sourceDir + 'js/**/*.html'
          , '.htmlhintrc'
        ]
    }
};
