#!/bin/bash

cd `echo $(cd $(dirname $0);pwd)`

ln -sf ../../src/images/icon-128.png .

ln -sf ../../../lazy-applicant-support/en/feature-for-redmine/images/1280x800-post-introduction-issue-basic.png    1280x800-post-introduction-issue-basic-en.png
ln -sf ../../../lazy-applicant-support/en/feature-for-redmine/images/1280x800-post-introduction-options-basic.png  1280x800-post-introduction-options-basic-en.png
ln -sf ../../../lazy-applicant-support/en/feature-for-redmine/images/1280x800-post-introduction-issue-expand.png   1280x800-post-introduction-issue-expand-en.png
ln -sf ../../../lazy-applicant-support/en/feature-for-redmine/images/1280x800-post-introduction-options-expand.png 1280x800-post-introduction-options-expand-en.png

ln -sf ../../../lazy-applicant-support/jp/feature-for-redmine/images/1280x800-post-introduction-issue-basic.png    1280x800-post-introduction-issue-basic-jp.png
ln -sf ../../../lazy-applicant-support/jp/feature-for-redmine/images/1280x800-post-introduction-options-basic.png  1280x800-post-introduction-options-basic-jp.png
ln -sf ../../../lazy-applicant-support/jp/feature-for-redmine/images/1280x800-post-introduction-issue-expand.png   1280x800-post-introduction-issue-expand-jp.png
ln -sf ../../../lazy-applicant-support/jp/feature-for-redmine/images/1280x800-post-introduction-options-expand.png 1280x800-post-introduction-options-expand-jp.png
