# README #

怠惰な申請者.  
Redmineのチケットが簡単に登録できるようになるChromeプラグインです.


## 利用者向け資料
[こちら](https://bitbucket.org/dhun-h/lazy-applicant-support)


## 開発者向け資料

### 開発環境構築

#### node

[nvm](https://github.com/creationix/nvm#install-script) をインストール

```shell
nvm install v0.12.7     # 最新を指定する
nvm alias default current
```

#### npm (nodeをインストール済みの場合)

```shell
npm update -g npm
```

#### gulp

```shell
npm install -g gulp
```

#### Lazy Applicant

```shell
git clone ...
cd ${ルート・ディレクトリ}
npm install

cp gulp/config-env.js-sample gulp/config-env.js
vi gulp/config-env.js # see 設定#gulp/config-env.js


cp src/js/env.js-sample src/js/env.js
cp src/js/env.js-sample src/js/env-release.js
vi src/js/env*.js # see 設定#src/js/env*.js
```

### gulpタスク

| タスク | コマンド | 概要 | Output |
|---|---|---|---|
|{ default }|gulp|JSとHTMLを監視してLintする||
|build|gulp build|.crxと.zipファイルをビルドする. 出力先はpublish/{crx,zip}/|publish/crx/xxx.{crx,md5}, publish/zip/xxx.zip|
|assert.js||ビルドに必要な定義が揃っているかを検証する||
|lint-js.js||JSファイルをLintする||
|lint-html.js||HTMLファイルをLintする||
|clean.js||publish/build/${version}/を削除する||
|copy.js||publish/build/${version}/へファイルをコピーする||
|switch-env.js||src/js/{env.js,env-release.js}を切り替える. config-env.jsに従う||
|package.js||publish/build/XXX.crxファイルを生成する||
|move-crx.js||publish/crx/にXXX.crxファイルを移動する||
|gen-latest.js||publish/crx/に-latest.crxを生成する||
|gen-digest.js||publish/crx/にXXX.md5を生成する||
|gen-zip.js||publish/zip/にXXX.zipを生成する||
|complete.js||build処理が正常終了したことをデスクトップ通知する||


### ビルド

#### 初回

1. chrome拡張機能タブを表示
1. デベロッパーモードをチェック
1. 拡張機能のパッケージ化...をクリック
1. 拡張機能のパッケージ化ボタンをクリック
    * 拡張機能のルートディレクトリ ... ${ルート・ディレクトリ}/src
    * 秘密鍵ファイル ... 空白
1. 生成された鍵ファイルを、次の名前にリネーム  
   ⇒ ${ルート・ディレクトリ}/publish/lazy-applicant.pem
1. ビルドしたファイルが出力される  
   ⇒ publish/zip/lazy-applicant-2.0.0.zip　※このファイルをWebStoreへアップロード  
   ⇒ publish/crx/lazy-applicant-2.0.0.crx　※独自ホスティングしなくなったため、これらのファイルは不要になった  
   ⇒ publish/crx/lazy-applicant-2.0.0.md5  
   ⇒ publish/crx/lazy-applicant-latest.crx  
   ⇒ publish/crx/lazy-applicant-latest.md5  

#### ２回め以降
```shell
cd ${ルート・ディレクトリ}
gulp build
```

### 設定

#### gulp/config-env.js

ビルド設定.

| 項目 | 設定値 | 説明 | 補足 |
|---|---|---|---|
| profile | 'release' or 'develop' | 'release'にした場合は、src/js/env-release.jsをsrc/js/env.jsとみなしてビルドする | |
| chrome | '/Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome' など | Chromeのインストールパス | .crxの生成で利用 |

#### src/js/env*.js

ランタイム設定.

| 項目 | 設定値 | 説明 | 補足 |
|---|---|---|---|
| DEBUG | true or false | デバッグモードかどうか | 現時点ではブラウザコンソールにログ出力するかどうかだけを制御 |
| GA_TRACKING_ID | 'UA-nnnnnnnn-n' | GoogleAnalyticsトラッキングID | リリース版では、GAを無効化している |
| CHROME_STORAGE | 'local' or 'sync' | プラグイン設定を保存するストレージ | リリース版では、'local' |
